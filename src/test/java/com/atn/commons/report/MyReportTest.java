package com.atn.commons.report;

import static org.junit.Assert.assertNotNull;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.atn.commons.TestsConfig;
import com.atn.commons.utils.FileManager;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={TestsConfig.class})
@WebAppConfiguration
public class MyReportTest {
	@Autowired
	private ReportEngine reportEngine;
	
	@Before
	public void setUp() throws Exception {
		reportEngine.startup();
	}
	
	@Test
	public void test() {
		assertNotNull(reportEngine);
	}
	@Test
	public void testGenerateWithoutParam() throws Exception {
		String report = "/new_report.rptdesign";
		OutputStream out = new FileOutputStream("target/test___Commons_new_report.doc");
		reportEngine.generate(report, out, ReportEngine.REPORT_TYPE.DOC);
	}
	@Test
	public void testGenerateWitParam() throws Exception {
		String report = "/hello_world.rptdesign";
		Map<String,Object> map = new HashMap<>();
		map.put("sample", "walid_commons");
		OutputStream out = new FileOutputStream("target/test__Commons_new_report.pdf");
		reportEngine.generate(report, out, ReportEngine.REPORT_TYPE.PDF,map);
	}

}
