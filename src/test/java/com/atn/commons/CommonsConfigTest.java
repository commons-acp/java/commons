package com.atn.commons;

import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.junit.*;
import org.junit.contrib.java.lang.system.ProvideSystemProperty;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { CustomTestConfig.class })
@WebAppConfiguration
@EnableAspectJAutoProxy
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CommonsConfigTest extends CustomTestConfig{

    @Value("${test}")
    private String testString;


    @Test
    public void setEnvironmentTest() {
        assertEquals(CustomTestConfig.absolutePath, System.getenv("contextConfigurationPath"));
        System.out.println(testString);

    }


    @Test
    public void testValueAfterContextLoad(){
        assertEquals(testString, "this is the test properties file");

    }
}
