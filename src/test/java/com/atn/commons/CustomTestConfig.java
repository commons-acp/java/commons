package com.atn.commons;

import org.junit.ClassRule;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.test.context.web.WebAppConfiguration;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;

@Configuration
@WebAppConfiguration
public class CustomTestConfig extends CommonsConfig {
    public static URL res = CustomTestConfig.class.getClassLoader().getResource("config-test.properties");
    public static File file;

     static {
        try {
            file = Paths.get(res.toURI()).toFile();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public static  String absolutePath = file.getAbsolutePath();
    @ClassRule
    public static final EnvironmentVariables environmentVariables
            = new EnvironmentVariables().set("contextConfigurationPath", absolutePath );



}
