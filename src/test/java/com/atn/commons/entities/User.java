package com.atn.commons.entities;

import com.atn.commons.mangopay.entities.UserMangoPay;
import lombok.Data;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "USER")
@Data
public class User extends ModelObject<Long> implements UserMangoPay   {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "USER_ID", unique = true, nullable = false)
    private Long id;
    @Column(name = "email")
    private String email;
    @Column(name = "name")
    private String name;
    @Column (name = "USER_MANGO_PAY_ID")
    private String mangoPayId;
    @Override
    public String getMangoPayId() {
        return this.mangoPayId;
    }
}
