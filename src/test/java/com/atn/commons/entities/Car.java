package com.atn.commons.entities;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CAR")
@Data
public class Car extends ModelObject<Long> {
    @Id
    @Column(name = "id")
    private Long id;
    @Column(name = "company")
    private String company;
    @Column(name = "model")
    private String model;
    @Column(name = "price")
    private long price;
 

}
