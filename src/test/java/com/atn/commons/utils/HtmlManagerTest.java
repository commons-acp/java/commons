package com.atn.commons.utils;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.core.io.Resource;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

import static com.atn.commons.utils.HtmlManager.writeUsingOutputStream;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class HtmlManagerTest {
    private String finalChemain="target"+File.separator+"test-classes"+File.separator+"html"+File.separator;
    @Test
    public void TestwriteUsingOutputStream() throws IOException {


        String original="original.html";
        String destination="output.html";

        InputStream inputStream = new FileInputStream(new File(finalChemain+original));
        OutputStream outputStream = new FileOutputStream(new File(finalChemain+destination));
        Map<String, String> mapDataChange =new HashMap<>();
        mapDataChange.put("projet","COMMONS");
        mapDataChange.put("prenom", "AMRI");
        mapDataChange.put("nom","Houssem");
        writeUsingOutputStream(inputStream,mapDataChange,outputStream);

        StringBuilder contentBuilder = new StringBuilder();
        StringBuilder contentBuilder1 = new StringBuilder();
        Stream<String> stream = Files.lines( Paths.get(finalChemain+"output.html"), StandardCharsets.UTF_8);

        stream.forEach(s -> contentBuilder.append(s).append("\n"));
        Stream<String> stream1 = Files.lines( Paths.get(finalChemain+"expected.html"), StandardCharsets.UTF_8);
        stream1.forEach(s -> contentBuilder1.append(s).append("\n"));
        assertEquals(contentBuilder.toString(),contentBuilder1.toString());


    }



    //test exeption
    @Test(expected = FileNotFoundException.class)
    public void TestwriteUsingOutputStreamExeption() throws IOException {

        String original="xyz.html";
        String destination="output.html";

        InputStream inputStream = new FileInputStream(new File(finalChemain+original));
        OutputStream outputStream = new FileOutputStream(new File(finalChemain+destination));
        Map<String, String> mapDataChange =new HashMap<>();
        mapDataChange.put("projet","COMMONS");
        mapDataChange.put("prenom", "AMRI");
        mapDataChange.put("nom","Houssem");
        writeUsingOutputStream(inputStream,mapDataChange,outputStream);

        StringBuilder contentBuilder = new StringBuilder();
        StringBuilder contentBuilder1 = new StringBuilder();
        Stream<String> stream = Files.lines( Paths.get(finalChemain+"output.html"), StandardCharsets.UTF_8);

        stream.forEach(s -> contentBuilder.append(s).append("\n"));
        Stream<String> stream1 = Files.lines( Paths.get(finalChemain+"expected.html"), StandardCharsets.UTF_8);
        stream1.forEach(s -> contentBuilder1.append(s).append("\n"));
        assertEquals(contentBuilder.toString(),contentBuilder1.toString());


    }

    //test IOExeption
    @Test(expected = IOException.class)
    public void TestwriteUsingOutputStreamExeptionIO() throws IOException {

        String original="xyz.html";
        String destination="output.html";

        InputStream inputStream = new FileInputStream(new File(finalChemain+original));
        OutputStream outputStream = new FileOutputStream(new File(finalChemain+destination));
        Map<String, String> mapDataChange =new HashMap<>();
        mapDataChange.put("projet","COMMONS");
        mapDataChange.put("prenom", "AMRI");
        mapDataChange.put("nom","Houssem");
        writeUsingOutputStream(inputStream,mapDataChange,outputStream);

        StringBuilder contentBuilder = new StringBuilder();
        StringBuilder contentBuilder1 = new StringBuilder();
        Stream<String> stream = Files.lines( Paths.get(finalChemain+"output.html"), StandardCharsets.UTF_8);

        stream.forEach(s -> contentBuilder.append(s).append("\n"));
        Stream<String> stream1 = Files.lines( Paths.get(finalChemain+"expected.html"), StandardCharsets.UTF_8);
        stream1.forEach(s -> contentBuilder1.append(s).append("\n"));
        assertEquals(contentBuilder.toString(),contentBuilder1.toString());


    }


}
