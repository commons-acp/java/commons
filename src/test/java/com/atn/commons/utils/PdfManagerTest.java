package com.atn.commons.utils;

import com.itextpdf.text.DocumentException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Value;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class PdfManagerTest {
    @Value("db.script.folder")
    private String metadata;

    @Test
    public void getNumberPagesTest() throws IOException {
        String filepath = "target/test-classes/files/file_test.pdf";
        int count = PdfManager.getNumberPages(filepath);
        Assert.assertEquals(15, count);
    }

    @Test
    public void exportOnePageTest() throws IOException {
        String filepath = "target/test-classes/files/file_test.pdf";
        int page = 0;
        String outpuFilePath = "target/outpu-" + page + ".pdf";
        File outputFile = new File(outpuFilePath);
        File pdfFile = new File(filepath);
        PdfManager.exportPage(pdfFile, page, outputFile);
        double size = (double) outputFile.length() / 1024;
        Assert.assertTrue(size > 630);
        Assert.assertTrue(size < 640);
    }

    @Test
    public void exportMultiplePagesTest() throws IOException {
        String filepath = "target/test-classes/files/file_test.pdf";
        int from = 0;
        int to = 3;
        String outpuFilePath = "target/outpu-" + from + "-" + to + ".pdf";
        File outputFile = new File(outpuFilePath);
        File pdfFile = new File(filepath);
        PdfManager.exportPage(pdfFile, from, to, outputFile);
        double size = (double) outputFile.length() / 1024;
        Assert.assertTrue(size > 640);
        Assert.assertTrue(size < 650);
    }

    @Test
    public void readPdfTest() throws IOException {
        String filepath = "target/test-classes/files/file_test.pdf";
        int page = 1;
        String outpuFilePath = "target/outpu-" + page + ".pdf";
        File outputFile = new File(outpuFilePath);
        File pdfFile = new File(filepath);
        PdfManager.exportPage(pdfFile, page, outputFile);
        String content = PdfManager.readText(outputFile, page);
        Assert.assertTrue(content.contains("Histoires drôles"));
    }

    @Test
    public void fusionTest() throws IOException {
        File file1 = new File("target/test-classes/files/file1.pdf");
        int file1_initial_pages = PdfManager.getNumberPages(file1.getAbsolutePath());
        File file2 = new File("target/test-classes/files/file2.pdf");
        file1 = PdfManager.fusion(file1, file2);
        Assert.assertEquals(file1_initial_pages + PdfManager.getNumberPages(file2.getAbsolutePath()), PdfManager.getNumberPages(file1.getAbsolutePath()));
    }

    @Test
    public void image2pdfTest() throws IOException {
        File image = new File("target/test-classes/files/chevre.jpg");
        File pdf = PdfManager.image2pdf(image);
        Assert.assertEquals(PdfManager.getNumberPages(pdf.getAbsolutePath()), 1);
    }
}
