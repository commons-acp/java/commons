package com.atn.commons.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.atn.commons.TestsConfig;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestsConfig.class })
@WebAppConfiguration

public class PropertiesManagerTest {

	@Test
	public void createPropertiesFromClassPathTest() throws IOException {
		String propFileName = "config.properties";

		InputStream inputStream = getClass().getClassLoader()
				.getResourceAsStream(propFileName);

		Properties properties = PropertiesManager.createProperties(inputStream);
		inputStream.close();
		Assert.assertEquals(properties.getProperty("log.environement"), "PROD");
	}

	@Test
	public void createPropertiesFromFileTest() throws IOException {
		String propFileName = "src/test/resources/config.properties";

		InputStream inputStream = new FileInputStream(new File(propFileName));

		Properties properties = PropertiesManager.createProperties(inputStream);
		Assert.assertEquals(properties.getProperty("log.environement"), "PROD");
	}

	@Test
	public void allConfigTest() throws IOException {
		List<String> files = getResourceFiles("/");
		for (int i = 0; i < files.size(); i++) {
			String fileSource = files.get(i);
			if (!fileSource.endsWith("config.proeprties"))
				continue;
			for (int j = i + 1; j < files.size(); j++) {
				String fileDestination = files.get(j);
				if (!fileDestination.endsWith("config.proeprties"))
					continue;
				InputStream sourceStream = getResourceAsStream(fileSource);
				InputStream destinationStream = getResourceAsStream(fileDestination);
				Assert.assertEquals(PropertiesManager.notInDestinationFieldList(sourceStream, destinationStream).size(),0);
				Assert.assertEquals(PropertiesManager.notInDestinationFieldList(destinationStream, sourceStream).size(),0);

			}
		}
	}

	protected List<String> getResourceFiles(String path) throws IOException {
		List<String> filenames = new ArrayList<>();

		try (InputStream in = getResourceAsStream(path);
				BufferedReader br = new BufferedReader(
						new InputStreamReader(in))) {
			String resource;

			while ((resource = br.readLine()) != null) {
				filenames.add(resource);
			}
		}

		return filenames;
	}

	private InputStream getResourceAsStream(String resource) {
		final InputStream in = getContextClassLoader().getResourceAsStream(
				resource);

		return in == null ? getClass().getResourceAsStream(resource) : in;
	}

	private ClassLoader getContextClassLoader() {
		return Thread.currentThread().getContextClassLoader();
	}
}
