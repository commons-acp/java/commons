package com.atn.commons.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;

import com.google.zxing.ChecksumException;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.WriterException;

public class BarCodeManagerTest {

	@Test
	public void createBarCodeTest() throws WriterException, IOException {
		String qrCodeText = "0123456789";
		int size = 200;
		FileOutputStream outputStream = new FileOutputStream("target/barcode.png");
		BarCodeManager.writeQrCode(outputStream, qrCodeText, size, 0xFFFFFFFF, 0xFF000000, null);
	}

	@Test
	public void readBarCodeTest() throws FileNotFoundException {
		FileInputStream fis = new FileInputStream(new File("target/barcode.png"));
		String qrCodeText = "0123456789";
		String result;
		try {
			result = BarCodeManager.readQrCode(fis);
			Assert.assertEquals(qrCodeText, result);
			fis.close();
			fis = new FileInputStream(new File("target/barcode.png"));
			Assert.assertTrue(BarCodeManager.checkQrCode(qrCodeText, fis));
		} catch (NotFoundException | ChecksumException | FormatException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
