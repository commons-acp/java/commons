package com.atn.commons.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.junit.Assert;
import org.junit.Test;

public class FileManagerTest {
	@Test
	public void getExtensionTest(){
		String filePath = "target/getExtensionTest.extension";
		File file = new File(filePath);
		
		String extention = FileManager.getExtension(file.getAbsolutePath());
		Assert.assertEquals("extension", extention);
	}
	@Test
	public void copyFileTest() throws IOException{
		String filePath = "target/copyFileTest.src";
		String fileDestinationPath="target/copyFileTest.dest";
		File source = new File(filePath);
		OutputStream dest = new FileOutputStream(new File(fileDestinationPath));
		String contentSource = "lookAtMe";
		FileManager.writeFile(source.getAbsolutePath(), contentSource);
		FileManager.copyFile(source, dest);
		String contentDest = FileManager.readFile(new File(fileDestinationPath));
		Assert.assertEquals(contentSource, contentDest);
		
	}
	@Test
	public void getResourceFilesTest() throws IOException {


		Assert.assertEquals(3,FileManager.getResourceFiles("/metadata","sql").length);
	}
}
