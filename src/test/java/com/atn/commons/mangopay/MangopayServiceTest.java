package com.atn.commons.mangopay;


import com.atn.commons.entities.User;
import com.atn.commons.mangopay.service.CommonsMangoPayService;

public interface MangopayServiceTest extends CommonsMangoPayService<User, Long> {
}
