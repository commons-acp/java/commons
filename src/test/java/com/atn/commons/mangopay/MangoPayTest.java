package com.atn.commons.mangopay;

import com.atn.commons.TestsConfig;
import com.mangopay.MangoPayApi;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestsConfig.class})
public class MangoPayTest {

    @Autowired
    private MangopayServiceTest service;

    @Test
    public void ApiAndUsersConstructionTest() {
        MangoPayApi mangoPayApi = service.getApiConnection();
        Assert.assertEquals("https://api.sandbox.mangopay.com" , mangoPayApi.getConfig().getBaseUrl());
        Assert.assertEquals("newClient", mangoPayApi.getConfig().getClientId());
        Assert.assertEquals("Has5/trp90@kfj" ,mangoPayApi.getConfig().getClientPassword() );
        Assert.assertNotNull(mangoPayApi);
        Assert.assertNotNull(mangoPayApi.getUserApi());
    }

}
