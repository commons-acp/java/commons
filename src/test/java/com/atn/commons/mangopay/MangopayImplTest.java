package com.atn.commons.mangopay;

import com.atn.commons.entities.User;
import com.atn.commons.mangopay.service.CommonsMangopayServiceImpl;
import com.mangopay.entities.BankAccount;
import com.mangopay.entities.Transfer;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class MangopayImplTest extends CommonsMangopayServiceImpl<User, Long> implements MangopayServiceTest {


    public MangopayImplTest(@Value("${commons.mango.api.client}") String clientId, @Value("${commons.mango.api.token}") String apiToken, @Value("${commons.mango.api.url:https://api.sandbox.mangopay.com}")
            String baseUrl) {
        super(clientId, apiToken, baseUrl);
    }

    @Override
    public String getClienId() {
        return clientId;
    }

    @Override
    public String getApiToken() {
        return apiToken;
    }

    @Override
    public String getBaseUrl() {
        return baseUrl;
    }


}
