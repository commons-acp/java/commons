package com.atn.commons.mangopay;

import com.atn.commons.TestsConfig;
import com.atn.commons.entities.User;
import com.atn.commons.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestsConfig.class })

public class UserMangoPayTest {

    @Autowired
    private UserService userService;


    @Test
    public void ContextLoad() {
        assertNotNull(userService);
    }

    @Test
    @Transactional
    public void mappingUserMango() {
        User user = new User();
        user.setId(1l);
        user.setEmail("m@m.m");
        user.setMangoPayId("mango@email.com");
        userService.save(user);
        User reponseUser = userService.findById(1l);
        assertEquals(reponseUser, user);
        assertEquals("mango@email.com", reponseUser.getMangoPayId());


    }
}
