package com.atn.commons.email;

import static org.junit.Assert.assertNotNull;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import com.atn.commons.EmailConfig;
import com.atn.commons.utils.FileManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.atn.commons.TestsConfig;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.HashMap;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={EmailConfig.class})
public class EmailSenderEngineTest {
	private static Logger logger = LogManager.getLogger(EmailSenderEngineTest.class);
	@Autowired
	private EmailHandler EmailHandler;
	
	private String destination="walid.mansia@gmail.com";
	@Before
	public void setUp() throws Exception {
	}
	@Test//(timeout=10000)
	public void test() throws AddressException, MessagingException, InterruptedException {
		assertNotNull(EmailHandler);
		//OBJETMAIL
		EmailHandler.send(destination, "Object", "message");
	}
	@Test//(timeout=10000)
	public void testwithAttachment() throws AddressException, MessagingException, InterruptedException, FileNotFoundException {
		assertNotNull(EmailHandler);
		String filepath="target/test-classes/files/file_test.pdf";

		String fileName="walid.pdf";
		//String fileContent="application/pdf";
		HashMap<File,String> fileMap = new HashMap<>();
		fileMap.put(new File(filepath),fileName);
		//pdf.
		//OBJETMAIL
		String bcc=null;
		String cci="walid.mansi@yayi.eu";
		EmailHandler.send(destination,bcc,cci, "Object", "message avec fichier",fileMap);
	}
	@Test
	public void sendGeneratedMailsTest() throws Exception {
		String inFolderPath = "target/mail-tmp/";
		String archiveFolderPath = "target/mail-tmp-arch/";
		String errorFolderPath = "target/mail-tmp-err/";
		String configFileFolderPath = "target/mail-tmp-conf/";
		FileManager.forceMkdir(inFolderPath);
		FileManager.forceMkdir(archiveFolderPath);
		FileManager.forceMkdir(errorFolderPath);
		FileManager.forceMkdir(configFileFolderPath);
		File config= new File("target/test-classes/config.mail.properties");
		File targetConfig = new File(configFileFolderPath+"config.mail.properties");
		FileManager.copyFile(config,new FileOutputStream(targetConfig));
		String [] args = {inFolderPath,archiveFolderPath,errorFolderPath,configFileFolderPath};
		FileSenderEngineHandler.main(args);

	}

}
