package com.atn.commons.sms;

import com.atn.commons.SmsConfig;
import com.atn.commons.sms.exceptions.InvalidRouteeCredential;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;

import static org.junit.Assert.assertNotNull;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={SmsConfig.class})
public class SmsEngineTest {
    @Autowired
    SmsHandler smsHandler;


    @Test
    public void test() throws IOException {
        assertNotNull(smsHandler);
        smsHandler.send("message de test","+21624658343");

    }


}
