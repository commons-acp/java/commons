package com.atn.commons;


import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@EnableWebMvc
@WebAppConfiguration
public class MockMvcConfig extends CommonsConfig {
}
