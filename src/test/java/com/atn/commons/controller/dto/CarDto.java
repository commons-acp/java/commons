package com.atn.commons.controller.dto;

import lombok.Data;

import javax.xml.bind.annotation.XmlElement;

@Data
public class CarDto extends ModelDto<Long> {

    @XmlElement
    private Long id;
    @XmlElement
    private String company;
    @XmlElement
    private String model;
    @XmlElement
    private long price;

    @Override
    public Long getId() {
        return this.id;
    }
}
