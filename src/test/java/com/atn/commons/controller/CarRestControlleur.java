package com.atn.commons.controller;


import com.atn.commons.controller.dto.CarDto;
import com.atn.commons.controller.rest.MyRestController;
import com.atn.commons.entities.Car;
import com.atn.commons.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/cars")
public class CarRestControlleur extends MyRestController<CarService, CarDto,Car,Long> {

    public CarRestControlleur(@Autowired CarService carService) {
        super(carService);
    }

    @Override
    protected Car mapperFromDto(CarDto dto) {
        Car model=new Car();
        model.setId(dto.getId());
        model.setPrice(dto.getPrice());
        model.setCompany(dto.getCompany());
        model.setModel(dto.getModel());
        return model;
    }

    @Override
    protected CarDto mapperToDto(Car model) {
        CarDto dto = new CarDto();
        dto.setId(model.getId());
        dto.setCompany(model.getCompany());
        dto.setModel(model.getModel());
        dto.setPrice(model.getPrice());
        return dto;
    }


}




