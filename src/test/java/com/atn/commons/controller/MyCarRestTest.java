package com.atn.commons.controller;



import com.atn.commons.MockMvcConfig;
import com.atn.commons.controller.dto.CarDto;
import com.atn.commons.controller.dto.ModelDto;
import com.atn.commons.entities.Car;
import com.atn.commons.entities.User;
import com.atn.commons.exception.ApiException;
import com.atn.commons.exception.BodyErrors;
import com.atn.commons.exception.HttpErrorResponse;
import com.atn.commons.service.CarService;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;


import javax.xml.bind.annotation.XmlElement;
import java.io.IOException;
import java.nio.charset.Charset;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { MockMvcConfig.class })
@WebAppConfiguration
public class MyCarRestTest  {
    public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8")
    );
    @Autowired
    private CarService carService;
    @Autowired
    private WebApplicationContext webApplicationContext;

    protected String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }
    protected <T> T mapFromJson(String json, Class<T> clazz)
            throws JsonParseException, JsonMappingException, IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, clazz);
    }



    @Test
    public void getCarsList() throws Exception {
        MockMvc  mvc= MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

        String uri = "/cars/?offset=1&size=1";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        System.out.println(mvcResult.getResponse().getStatus());
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        CarDto[] carList = mapFromJson(content, CarDto[].class);
        assertTrue(carList.length > 0);
    }
    @Test
    public void getCar() throws Exception {
        MockMvc  mvc= MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        String uri = "/cars/1";
        mvc.perform(get(uri))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
        //.andExpect(jsonPath("$", hasSize(1)))
        //.andExpect(jsonPath("$[0].id", is(1)))
                /*.andExpect(jsonPath("$[0].description", is("Lorem ipsum")))
                .andExpect(jsonPath("$[0].title", is("Foo")))
                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[1].description", is("Lorem ipsum")))
                .andExpect(jsonPath("$[1].title", is("Bar")))*/
        ;

    }

    @Test
    @Ignore("shoudl create before delete, if not pb with other tests")
    public void deleteCar() throws Exception {
        MockMvc  mvc= MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        String uri = "/cars/1";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(uri)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        assertEquals(content, "Success");
        //System.out.println(carService.findById(1L).getDeletingDate());

    }
    @Test
    //@Ignore
    public void updateCar() throws Exception {
        MockMvc  mvc= MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        String uri = "/cars/1";
        Car car = carService.findById(1L);
        car.setModel("Lemon");
        CarDto dto = new CarDto();
        dto.setId(car.getId());
        dto.setCompany(car.getCompany());
        dto.setModel(car.getModel());
        dto.setPrice(car.getPrice());
        String inputJson = mapToJson(dto);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put(uri)
                .contentType(APPLICATION_JSON_UTF8)
                .content(inputJson)).andReturn();
        Car carTwo = carService.findById(1L);
        int status = mvcResult.getResponse().getStatus();
        CarDto dto2 = new CarDto();
        dto2.setId(carTwo.getId());
        dto2.setCompany(carTwo.getCompany());
        dto2.setModel(carTwo.getModel());
        dto2.setPrice(carTwo.getPrice());
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        assertEquals(content, mapToJson(dto2));
        System.out.println(carService.findById(1L));
    }
    @Test
    public void createCar() throws  Exception{
        MockMvc  mvc= MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        String uri = "/cars/";
        Car carToAdd = carService.createCar(new Long(3));
        carToAdd.setModel("XXXX");
        carToAdd.setCompany("YYYY");
        carToAdd.setPrice(1000L);

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(APPLICATION_JSON_UTF8)
                .content(mapToJson(carToAdd))).andReturn();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        // String content = mvcResult.getResponse().getContentAsString();

    }
    @Test
    public void getAllCarsList() throws Exception {
        MockMvc  mvc= MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

        String uri = "/cars/?offset=0&size=0";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        System.out.println(mvcResult.getResponse().getStatus());
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        CarDto[] carList = mapFromJson(content, CarDto[].class);
        assertTrue(carList.length == 2);
    }
    @Test
    public void getCarsListBadParams() throws Exception {
        MockMvc  mvc= MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

        String uri = "/cars/?offset=0&size=1";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        System.out.println(mvcResult.getResponse().getStatus());
        assertEquals(400, status);
        String content = mvcResult.getResponse().getContentAsString();
        HttpErrorResponse response = mapFromJson(content, HttpErrorResponse.class);
        assertEquals(response.getMessage(),"wrong values for offset and size params");
        assertEquals(response.getCode(),997);
    }
    @Test
    public void getCarsListNegativeParams() throws Exception {
        MockMvc  mvc= MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

        String uri = "/cars/?offset=-3&size=1";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        System.out.println(mvcResult.getResponse().getStatus());
        assertEquals(400, status);
        String content = mvcResult.getResponse().getContentAsString();
        HttpErrorResponse response = mapFromJson(content, HttpErrorResponse.class);
        assertEquals(response.getMessage(),"offset and size params can't be negative");
        assertEquals(response.getCode(),997);
    }
    @Test
    public void getCarNotFound() throws Exception {
        MockMvc  mvc= MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

        String uri = "/cars/0";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        System.out.println(mvcResult.getResponse().getStatus());
        assertEquals(404, status);
        String content = mvcResult.getResponse().getContentAsString();
        HttpErrorResponse response = mapFromJson(content, HttpErrorResponse.class);
        assertEquals(response.getMessage(),"Entity "+Car.class.getSimpleName()+" NotFound");
        assertEquals(response.getCode(),998);
    }
    @Test
    public void createCarBadRequest() throws  Exception{
        MockMvc  mvc= MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        String uri = "/cars/";
        Car carToAdd = carService.createCar(new Long(3));
        User object = new User();
        object.setId(new Long(500));
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(APPLICATION_JSON_UTF8)
                .content("item: param")).andReturn();
        int status = mvcResult.getResponse().getStatus();
        String content = mvcResult.getResponse().getContentAsString();
        System.out.println(content);
        assertEquals(400, status);
        HttpErrorResponse response = mapFromJson(content, HttpErrorResponse.class);
        assertEquals(response.getMessage(),"JSON Body Error , can't be Serialized");
        assertEquals(response.getCode(), BodyErrors.JSON_BODY_ERROR.getCode());


    }

}
