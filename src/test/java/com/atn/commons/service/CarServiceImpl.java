package com.atn.commons.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.atn.commons.entities.Car;
import com.atn.commons.persistence.BaseDao;
import com.atn.commons.persistence.jpa.CarDao;
@Component
public class CarServiceImpl extends MyAbstractService<Car,Long> implements CarService{
	public CarServiceImpl(@Autowired CarDao carDao) {
		super(carDao);
	}

	@Override
	public CarDao getDataAccessObject() {
		return (CarDao) dataAccessObject;
	}
	@Transactional
	public Car createCar(Long id){
		Car car = new Car();
		car.setCompany("Mercedes");
		car.setModel("Class C");
		car.setPrice(new Long(1000));
		car.setId(id);
		save(car);
		return car;
	}
	public Car getFromCache(Long primaryKey){
		return getDataAccessObject().getFromCache(primaryKey);
	}
}
