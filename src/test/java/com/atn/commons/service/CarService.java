package com.atn.commons.service;

import com.atn.commons.entities.Car;

public interface CarService extends MyService<Car,Long>{
	public Car createCar(Long id);
	public Car getFromCache(Long primaryKey);
}
