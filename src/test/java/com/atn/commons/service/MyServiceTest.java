package com.atn.commons.service;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.atn.commons.entities.User;
import com.atn.commons.exception.MaxSizeExceededException;
import com.atn.commons.exception.NotFoundException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.atn.commons.TestsConfig;
import com.atn.commons.entities.Car;
import com.atn.commons.persistence.BaseDao;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestsConfig.class })
public class MyServiceTest {
	@Autowired
	private CarService carService;

	@Test
	public void test() {
		assertNotNull(carService);
	}

	@Transactional
	@Test
	public void testDelete()  {
		Car car = carService.findById(new Long(1));
		carService.delete(car);
		carService.flush();
		Map<String, Object> map = new HashMap<String, Object>();
		List<Car> cars = carService.findListByCriteria(map, 0, 10, BaseDao.DELETION_STATUS.ACTIVE);
		Assert.assertEquals(cars.isEmpty(), true);

	}

	@Test
	public void testCreate() {
		Car car = carService.createCar(new Long(3));
		// carService.flush();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("=company", car.getCompany());
		car = carService.findById(new Long(3));

		Assert.assertNotNull(car);

		// carService.save(car);

	}

	@Transactional
	@Test
	public void testUpdate() {
		Car car = carService.createCar(new Long(4));
		carService.flush();
		car = carService.findById(new Long(4));
		car.setCompany("walid");
		carService.save(car);
		carService.flush();
		car = carService.findById(new Long(4));
		Assert.assertEquals("walid", car.getCompany());
	}
	@Test
	public void testWithoutCache(){
		Car car = carService.createCar(new Long(5));
		//carService.flush();
		Car carOne = carService.findById(new Long(5));
		Assert.assertTrue("Mercedes".equals(carOne.getCompany()));
		Car carTwo = carService.findById(new Long(5));
		
		Assert.assertNotSame(carOne, carTwo);
		
		carTwo = carService.findById(new Long(5));
		Assert.assertNotSame(carOne, carTwo);
	}
	@Test
	@Transactional
	public void testWithCache(){
		Car car = carService.createCar(new Long(6));
		carService.flush();
		Car carOne = carService.getFromCache(new Long(6));
		Assert.assertTrue("Mercedes".equals(carOne.getCompany()));
		Car carTwo = carService.getFromCache(new Long(6));
		
		Assert.assertSame(carOne, carTwo);
		
		carTwo = carService.getFromCache(new Long(6));
		Assert.assertSame(carOne, carTwo);
	}

	@Test
	public void testMaxSizeExceededException()  {
		Map<String,Object> map = new HashMap<>();
		assertNull(carService.findListByCriteria(map,201,0, BaseDao.DELETION_STATUS.ACTIVE));
	}

	@Test(expected = NotFoundException.class)
	public void testNotFoundException(){
		Car car = carService.findById(new Long(100));
	}


}
