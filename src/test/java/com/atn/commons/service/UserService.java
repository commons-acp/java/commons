package com.atn.commons.service;

import com.atn.commons.entities.User;

public interface UserService extends MyService<User,Long>{
    public User createUser(Long id);
    public User getFromCache(Long primaryKey);
}
