package com.atn.commons.service;

import com.atn.commons.entities.User;
import com.atn.commons.persistence.jpa.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class UserServiceImpl extends MyAbstractService<User, Long> implements UserService {
    public UserServiceImpl(@Autowired UserDao UserDao) {
        super(UserDao);
    }

    @Override
    public UserDao getDataAccessObject() {
        return (UserDao) dataAccessObject;
    }

    @Transactional
    public User createUser(Long id) {
        User user = new User();
        user.setEmail("Mercedes@m.com");
//        user.setMangoPayEmail("m@m.m");
        user.setId(id);
        save(user);
        return user;
    }

    public User getFromCache(Long primaryKey) {
        return getDataAccessObject().getFromCache(primaryKey);
    }
}
