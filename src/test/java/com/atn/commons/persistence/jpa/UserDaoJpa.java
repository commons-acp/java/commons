package com.atn.commons.persistence.jpa;

import com.atn.commons.entities.User;
import org.springframework.stereotype.Component;

import org.springframework.cache.annotation.Cacheable;
import java.util.HashMap;
import java.util.Map;

@Component
public class UserDaoJpa extends BaseDaoJpa<User, Long> implements UserDao {
    public UserDaoJpa() {
        super(User.class);
    }

    @Override
    public User get(Long primaryKey) {
        return super.get(primaryKey);
    }

    @Override
    @Cacheable("user")
    public User getFromCache(Long primaryKey) {
        return super.get(primaryKey);
    }

    public Long getMaxId() {
        Long result = Long.parseLong("0");
        String query = "select max(c.id) from Car c";

        Map<String, Object> map = new HashMap<>();
        result = getSingleResult(Long.class, query, map);
        return result;

    }


}
