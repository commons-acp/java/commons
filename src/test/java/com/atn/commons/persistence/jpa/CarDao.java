package com.atn.commons.persistence.jpa;

import com.atn.commons.entities.Car;
import com.atn.commons.persistence.BaseDao;

public interface CarDao extends BaseDao<Car,Long> {
	public Car getFromCache(Long primaryKey);
	public Long getMaxId();
}
