package com.atn.commons.persistence.jpa;

import com.atn.commons.entities.User;
import com.atn.commons.persistence.BaseDao;

public interface UserDao extends BaseDao<User , Long> {
    public User getFromCache(Long primaryKey);
    public Long getMaxId();
}
