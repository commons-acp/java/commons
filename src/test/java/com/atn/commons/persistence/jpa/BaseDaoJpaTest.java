package com.atn.commons.persistence.jpa;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import com.atn.commons.exception.MaxSizeExceededException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.atn.commons.TestsConfig;
import com.atn.commons.entities.Car;
import com.atn.commons.persistence.BaseDao.DELETION_STATUS;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestsConfig.class })
@WebAppConfiguration()
public class BaseDaoJpaTest {

	@Autowired
	private CarDao myJpa;

	@Before
	public void setUp() throws Exception {
	}

	@Test
	@Transactional
	public void test() throws AddressException, MessagingException {
		assertNotNull(myJpa);
		Car car = new Car();
		car.setCompany("walid");
		car.setModel("modele walid");
		car.setId(new Long(10));
		myJpa.save(car);
		myJpa.flush();

		car.setModel("modele walid 2");
		myJpa.save(car);
		myJpa.flush();
		car = myJpa.findById(new Long(10));
		assertNotNull(car);
		Assert.assertNotEquals("modele walid", car.getModel());
		Assert.assertEquals("modele walid 2", car.getModel());

		car.setCompany("walid3");
		car.setModel("modele walid3");
		myJpa.persist(car);
		myJpa.flush();
		myJpa.delete(car);
		myJpa.flush();
	}

	@Transactional
	@Test
	public void findListByCriteria() throws MaxSizeExceededException {
		assertNotNull(myJpa);
		Car car = new Car();
		car.setCompany("walid");
		car.setModel("modele walid");
		car.setId(new Long(10));
		myJpa.save(car);
		myJpa.flush();

		Map<String, Object> map = new HashMap<String, Object>();
		 map.put("=company", "walid");

		List<Car> cars = myJpa.findListByCriteria(map, 10, 0,
				DELETION_STATUS.BOTH);
		Assert.assertEquals(cars.size(), 1);
		car = cars.get(0);
		 Assert.assertEquals("modele walid", car.getModel());
		 myJpa.delete(car);
		 cars = myJpa.findListByCriteria(map, 10, 0,
					DELETION_STATUS.ACTIVE);
			Assert.assertEquals(cars.size(), 0);
			cars = myJpa.findListByCriteria(map, 10, 0,
					DELETION_STATUS.DELETED);
			Assert.assertEquals(cars.size(), 1);
	}

	@Transactional
	@Test
	public void getCount() {
		assertNotNull(myJpa);

		Map<String, Object> map = new HashMap<String, Object>();
		int count = myJpa.getCount(map, DELETION_STATUS.ACTIVE);
		Assert.assertTrue(count>= 2);
	
	}

	@Transactional
	@Test
	@Ignore("for some reason i have a pb with a docker runner, TODO")
	public void getSingleResultTest(){
		Long maxId = myJpa.getMaxId();
        assertTrue(2 == maxId);
	}

	@Test(expected = MaxSizeExceededException.class)
	public void findListByCriteriaMaxSizeExceededException() throws MaxSizeExceededException {
		Map<String, Object> map = new HashMap<String, Object>();
		List<Car> cars = myJpa.findListByCriteria(map, 201, 0,
				DELETION_STATUS.BOTH);

	}
}
