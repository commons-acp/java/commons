package com.atn.commons.persistence.jpa;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import com.atn.commons.entities.Car;

import java.util.HashMap;
import java.util.Map;

@Component
public class CarDaoJpa extends BaseDaoJpa<Car, Long> implements CarDao {
	public CarDaoJpa() {
		super(Car.class);
	}

	@Override
	public Car get(Long primaryKey) {
		return super.get(primaryKey);
	}
	
	@Override
	@Cacheable("car")
	public Car getFromCache(Long primaryKey) {
		return super.get(primaryKey);
	}

	public Long getMaxId(){
		Long result=Long.parseLong("0");
		String query="select max(c.id) from Car c";

        Map<String, Object> map = new HashMap<>();
        result = getSingleResult(Long.class,query,map);
		return result;

	}
	

}
