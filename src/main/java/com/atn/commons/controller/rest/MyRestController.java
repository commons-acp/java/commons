package com.atn.commons.controller.rest;

import java.io.Serializable;
import java.util.*;

import com.atn.commons.controller.dto.ModelDto;
import com.atn.commons.entities.ModelObject;
import com.atn.commons.exception.BadRequestException;
import com.atn.commons.exception.BodyErrors;
import com.atn.commons.persistence.BaseDao;
import com.atn.commons.service.MyService;
import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;


@RestController
@Transactional
public abstract class MyRestController<D extends MyService<T, S>, M extends ModelDto<S>, T extends ModelObject<S>, S extends Serializable> {
    protected D service;

    public MyRestController(D service) {
        this.service = service;
    }

    protected D getService() {
        return service;
    }

    @Data
    protected class Response {
        final String info;
    }

    protected abstract T mapperFromDto(M dto);

    protected abstract M mapperToDto(T model);


    /**
     * @param offset <code>int</code> defines the number of the page
     * @param size   <code>int</code>  defines the size of the <code>List</code> to be returned MaxSize is 200
     * @return <code>List</code> of data transfer object <code>M</code>
     * @author Monta
     * @since v3.78
     */
    @GetMapping(value = "/", produces = {MediaType.APPLICATION_JSON_VALUE})
    public List<M> get(@RequestParam("offset") int offset,
                       @RequestParam("size") int size) {
        Map<String, Object> map = new HashMap<>();
        if (offset < 0 || size < 0) {
            throw new BadRequestException("offset and size params can't be negative", BodyErrors.BAD_PARAMS);
        }
        if (offset == 0) {
            if (size != 0) {
                throw new BadRequestException("wrong values for offset and size params", BodyErrors.BAD_PARAMS);
            } else {
                List<T> models = service.findListByCriteria(map, 200, 0, BaseDao.DELETION_STATUS.ACTIVE);
                List<M> dtos = new ArrayList<>();

                for (int i = 0; i < models.size(); i++) {
                    dtos.add(i, mapperToDto(models.get(i)));
                }
                return dtos;
            }
        }
        int start = size * (offset - 1);
        if (size > 200) {
            List<T> models = service.findListByCriteria(map, 200, start, BaseDao.DELETION_STATUS.ACTIVE);
            List<M> dtos = new ArrayList<>();
            for (int i = 0; i < models.size(); i++) {
                dtos.add(i, mapperToDto(models.get(i)));
            }

            return dtos;
        } else {
            List<T> models = service.findListByCriteria(map, size, start, BaseDao.DELETION_STATUS.ACTIVE);
            List<M> dtos = new ArrayList<>();
            for (int i = 0; i < models.size(); i++) {
                dtos.add(i, mapperToDto(models.get(i)));
            }

            return dtos;
        }

    }

    @GetMapping(value = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public M get(@PathVariable("id") S id) {

        return mapperToDto(service.findById(id));
    }


    @DeleteMapping(value = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity delete(@PathVariable("id") S id) {
        T model = service.findById(id);
        service.delete(model);
        return new ResponseEntity<>(new Response("Success"), HttpStatus.OK);
    }

    @PostMapping(value = "/", consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity post(@RequestBody M body) {
        T entity = mapperFromDto(body);
        service.save(entity);
        return new ResponseEntity<>(new Response("Success"), HttpStatus.OK);
    }

    @PutMapping(path = "/{id}", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = MediaType.APPLICATION_JSON_VALUE)
    public M put(@PathVariable("id") S id, @RequestBody M body) {
        T model = service.findById(id);
        Date date = model.getCreationDate();
        model = mapperFromDto(body);
        model.setUpdatingDate(date);
        service.save(model);
        return mapperToDto(model);

    }
}


