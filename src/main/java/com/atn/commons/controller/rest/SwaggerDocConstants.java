package com.atn.commons.controller.rest;

public interface SwaggerDocConstants {
  public static final String API_RESPONSE_CODE_200 = "OK-Successfully";
  public static final String API_RESPONSE_CODE_401 = "You are not authorized to view the resource";
  public static final String API_RESPONSE_CODE_403 = "Accessing the resource you were trying to reach is forbidden";
  public static final String API_RESPONSE_CODE_404 = "The resource you were trying to reach is not found";
  public static final String API_MODEL_PROPERTY_MODELDTO_REF_TRANSACTION = "The refTrasaction";
  
}
