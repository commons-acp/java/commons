package com.atn.commons.controller.dto;

import java.io.Serializable;



public abstract  class ModelDto  <S extends Serializable>  {

  public abstract S getId ();

  public ModelDto() {

  }
}
