package com.atn.commons.service.business;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.atn.commons.entities.ModelObject;
import com.atn.commons.exception.BusinessException;
import com.atn.commons.exception.MaxSizeExceededException;
import com.atn.commons.exception.TechnicalException;
import com.atn.commons.persistence.BaseDao;

public interface IService<T extends ModelObject<S>, S extends Serializable> {

	/**
	 * Use this method to load an entity instance that MUST exist. An exception
	 * will be thrown if the instance is not found.
	 * 
	 * @throws TechnicalException
	 */
	T get(S primaryKey) throws TechnicalException;

	/**
	 * @throws TechnicalException
	 */
	T findById(S primaryKey) throws TechnicalException;

	/**
	 * Cette methode permet de rajouter l'entité dans la bd
	 * 
	 * @throws TechnicalException
	 * @throws BusinessException
	 */
	void persist(T entity) throws TechnicalException, BusinessException;

	/**
	 * cette methode permet une suppression logique de l'entité en lui rajoutant
	 * une date de suppresseion
	 */
	void delete(T entity);

	/**
	 * Cette methode permet de mettre à jour une entité
	 * 
	 * @throws TechnicalException
	 */
	T save(T entity) throws TechnicalException;

	/**
	 * Cette methode permet de synchroniser le persistance contexte avec la BD
	 * 
	 * @throws TechnicalException
	 */
	void flush() throws TechnicalException;

	void clear();

	/**
	 * Search with criteria Map
	 * 
	 * @param clazz
	 *            the given object class
	 * @param map
	 *            the search attributes
	 * @param maxResults
	 *            max result size (put null for no limit)
	 * @param firstResult
	 *            the first element position
	 *
	 * @return List
	 * @throws TechnicalException
	 */
	List<T> findListByCriteria(Map<String, Object> map, Integer maxResults, Integer firstResult,
                               BaseDao.DELETION_STATUS status) throws TechnicalException, MaxSizeExceededException;

	int getCount(Map<String, Object> map, BaseDao.DELETION_STATUS status) throws TechnicalException;

}
