package com.atn.commons.service.business;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceException;
import javax.persistence.TransactionRequiredException;

import com.atn.commons.entities.ModelObject;
import com.atn.commons.exception.BusinessException;
import com.atn.commons.exception.MaxSizeExceededException;
import com.atn.commons.exception.TechnicalException;
import com.atn.commons.persistence.BaseDao;

/**
 * Tous les service metiers "non techniques" doivent heriter de cette classe et
 * ne doivent contenir aucun attribut à injecter. l'injection de la dao se fait
 * par le bias du constructeur
 * 
 * @author Mohamed Arbi Ben Slimane
 *
 * @Email benslimane.arbi@gmail.com
 *
 * @Date 16 avr. 2017 at 11:35:18
 * @param <T>
 * @param <S>
 */
public abstract class AbstractService<T extends ModelObject<S>, S extends Serializable> implements IService<T, S> {

	protected BaseDao<T, S> dataAccessObject;

	public AbstractService(BaseDao<T, S> dataAccessObject) {
		this.dataAccessObject = dataAccessObject;
	}

	public abstract BaseDao<T, S> getDataAccessObject();

	@Override
	public T get(S primaryKey) throws TechnicalException {
		try {
			final T t = dataAccessObject.get(primaryKey);
			return t;
		} catch (EntityNotFoundException entityNotFoundException) {
			throw new TechnicalException("L'entité recherchée est introuvable dans la table",
					entityNotFoundException.fillInStackTrace());
		} catch (IllegalArgumentException illegalArgumentException) {
			throw new TechnicalException("l'argument passé en parametre pose un probleme",
					illegalArgumentException.fillInStackTrace());
		}
	}

	@Override
	public T findById(S primaryKey) throws TechnicalException {
		try {
			final T t = dataAccessObject.findById(primaryKey);
			return t;
		} catch (IllegalArgumentException illegalArgumentException) {
			throw new TechnicalException("l'argument passé en parametre pose un probleme",
					illegalArgumentException.fillInStackTrace());
		}
	}

	@Override
	public void persist(T entity) throws TechnicalException, BusinessException {
		try {
			dataAccessObject.persist(entity);
		} catch (EntityExistsException entityExistsException) {
			throw new BusinessException("l'entité exsiste deja", entityExistsException.fillInStackTrace());
		} catch (IllegalArgumentException illegalArgumentException) {
			throw new TechnicalException("l'instance passé en parametre n'est pas une entité",
					illegalArgumentException.fillInStackTrace());
		} catch (TransactionRequiredException transactionRequiredException) {
			throw new TechnicalException("Pas de transaction trouvée", transactionRequiredException.fillInStackTrace());
		}
	}

	@Override
	public void delete(T entity) {
		dataAccessObject.delete(entity);
	}

	@Override
	public T save(T entity) throws TechnicalException {
		try {
			final T t = dataAccessObject.save(entity);
			return t;
		} catch (IllegalArgumentException illegalArgumentException) {
			throw new TechnicalException("l'instance passé en parametre n'est pas une entité ou a été supprimé",
					illegalArgumentException.fillInStackTrace());
		} catch (TransactionRequiredException transactionRequiredException) {
			throw new TechnicalException("Pas de transaction trouvée", transactionRequiredException.fillInStackTrace());
		}
	}

	@Override
	public void flush() throws TechnicalException {
		try {
			dataAccessObject.flush();
		} catch (TransactionRequiredException transactionRequiredException) {
			throw new TechnicalException("Pas de transaction trouvée", transactionRequiredException.fillInStackTrace());
		} catch (PersistenceException persistenceException) {
			throw new TechnicalException("l'operation de synchronisation n'a pas pu aboutir",
					persistenceException.fillInStackTrace());
		}
	}

	@Override
	public void clear() {
		dataAccessObject.clear();

	}

	@Override
	public List<T> findListByCriteria(Map<String, Object> map, Integer maxResults, Integer firstResult,
			BaseDao.DELETION_STATUS status) throws TechnicalException, MaxSizeExceededException {
		try {
			final List<T> result = dataAccessObject.findListByCriteria(map, maxResults, firstResult, status);
			return result;
		} catch (IllegalArgumentException illegalArgumentException) {
			throw new TechnicalException("la requete construite par criteria builer est invalide",
					illegalArgumentException.fillInStackTrace());
		} catch (PersistenceException persistenceException) {
			throw new TechnicalException(
					"l'execution de la requete a depassé le timeout setter dans la transaction ou la transaction est introuvable",
					persistenceException.fillInStackTrace());
		}
	}

	@Override
	public int getCount(Map<String, Object> map, BaseDao.DELETION_STATUS status) throws TechnicalException {
		try {
			final int count = dataAccessObject.getCount(map, status);
			return count;
		} catch (PersistenceException persistenceException) {
			throw new TechnicalException("Un probleme est survenu lors de la recuperation du nombre de ligne",
					persistenceException.fillInStackTrace());
		}
	}

}
