package com.atn.commons.service;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.atn.commons.entities.ModelObject;
import com.atn.commons.persistence.BaseDao;

public interface MyService<T extends ModelObject<S>, S extends Serializable> {

	void setDataAccessObject(BaseDao<T, S> baseDao);

	/**
	 * Use this method to laod an entity insatnce that MUST exist. An exception
	 * will be thrown if the instance is not found.
	 * 
	 * @see javax.persistence.EntityManager#getReference(Class, Object)
	 */
	public T get(S primaryKey);

	/**
	 * @see javax.persistence.EntityManager#find(Class, Object)
	 */
	public T findById(S primaryKey);

	/**
	 * @see javax.persistence.EntityManager#persist(Object)
	 */
	public void persist(T entity);

	/**
	 * @see javax.persistence.EntityManager#remove(Object)
	 */
	public void delete(T entity);

	/**
	 * @see javax.persistence.EntityManager#merge(Object)
	 */
	public T save(T entity);

	/**
	 * @see javax.persistence.EntityManager#flush()
	 */
	public void flush();

	public void clear();

	/**
	 * Search with criteria Map
	 * 
	 * @param clazz
	 *            the given object class
	 * @param map
	 *            the search attributes
	 * @param maxResults
	 *            max result size (put null for no limit)
	 * @param firstResult
	 *            the first element position
	 *
	 * @return List
	 */

	public List<T> findListByCriteria(Map<String, Object> map,
                                      Integer maxResults, Integer firstResult,
                                      BaseDao.DELETION_STATUS status) ;

	public int getCount(Map<String, Object> map, BaseDao.DELETION_STATUS status);

}
