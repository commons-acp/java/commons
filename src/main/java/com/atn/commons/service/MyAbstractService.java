package com.atn.commons.service;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Map;

import com.atn.commons.exception.BodyErrors;
import com.atn.commons.exception.MaxSizeExceededException;
import com.atn.commons.exception.NotFoundException;
import org.springframework.stereotype.Component;

import com.atn.commons.entities.ModelObject;
import com.atn.commons.persistence.BaseDao;

@Component
public abstract class MyAbstractService<T extends ModelObject<S>, S extends Serializable> implements MyService<T, S> {
    protected BaseDao<T, S> dataAccessObject;

    Class<T> clazz;

    @SuppressWarnings("unchecked")
    public MyAbstractService(BaseDao<T, S> dataAccessObject) {

        this.dataAccessObject = dataAccessObject;
        this.clazz = (Class<T>)
                ((ParameterizedType) getClass()
                        .getGenericSuperclass())
                        .getActualTypeArguments()[0];
    }


    protected abstract BaseDao<T, S> getDataAccessObject();

    @Override
    public void setDataAccessObject(BaseDao<T, S> dataAccessObject) {
        this.dataAccessObject = dataAccessObject;
    }

    @Override
    public T get(S primaryKey) {
        return dataAccessObject.get(primaryKey);
    }

    /**
     * <p> Updated to throw  a  NotFoundException in
     * case the entity is not found </p>
     *
     * @param primaryKey representing the Id of the entity
     * @return Entity
     */
    @Override
    public T findById(S primaryKey) {
        if (dataAccessObject.findById(primaryKey) == null) {

            throw new NotFoundException("Entity " + clazz.getSimpleName() + " NotFound", BodyErrors.NOT_FOUND);
        }
        return dataAccessObject.findById(primaryKey);
    }

    @Override
    public void persist(T entity) {
        dataAccessObject.persist(entity);
    }

    @Override
    public void delete(T entity) {
        dataAccessObject.delete(entity);
    }

    @Override
    public T save(T entity) {
        return dataAccessObject.save(entity);
    }

    @Override
    public void flush() {
        dataAccessObject.flush();
    }

    @Override
    public void clear() {
        dataAccessObject.clear();

    }

    @Override
    public List<T> findListByCriteria(Map<String, Object> map,
                                      Integer maxResults, Integer firstResult, BaseDao.DELETION_STATUS status) {
        try {
            return dataAccessObject
                    .findListByCriteria(map, maxResults, firstResult, status);

        } catch (MaxSizeExceededException e) {
            e.printStackTrace();
            return null;
        }


    }


    @Override
    public int getCount(Map<String, Object> map, BaseDao.DELETION_STATUS status) {
        return dataAccessObject.getCount(map, status);
    }
}
