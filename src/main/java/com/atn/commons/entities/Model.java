package com.atn.commons.entities;

import java.io.Serializable;
import java.util.Date;

/**
 * @author <a href="mailto:lotfi.rhouma@allence-tunisie.com">Lotfi RHOUMA</a>
 * @version $Id: Model.java 277 2013-01-29 10:15:35Z lotfi $
 * @since 1.0 - 17 janv. 2013 - 14:26:02
 */
public interface Model<S extends Serializable> {

	S getId();
	public void setDeletingDate(Date deletingDate);
	public void setUpdatingDate(Date updatingDate);
	public void setCreationDate(Date creationDate);
	public Date getCreationDate() ;
	public Date getUpdatingDate() ;
	public Date getDeletingDate();
}
