package com.atn.commons.persistence;

import com.atn.commons.utils.FileManager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.init.ScriptUtils;

import javax.sql.DataSource;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DBInitializer {
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplate;
    private static Logger logger = LogManager.getLogger(DBInitializer.class);

    private static DBInitializer ourInstance = null;

    public static DBInitializer getInstance(DataSource dataSource) throws SQLException {
        if(ourInstance == null){
            ourInstance = new DBInitializer(dataSource);
        }
        return ourInstance;
    }

    private DBInitializer(DataSource dataSource) throws SQLException {
        this.dataSource = dataSource;
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public static String getValidationQuery() {
        return "SELECT 1 FROM DB_VERSION";
    }

    public void update() throws Exception {
        createDbVersion();
        String foldername ="/metadata";
        List<String> doneScripts = new ArrayList<>();
        Resource[] todoScripts = FileManager.getResourceFiles(foldername,"sql");
        List<String> doneScriptIds = new ArrayList<>();
        //todoScripts.addAll(Arrays.asList(listFiles));
        while(todoScripts.length>doneScripts.size()){
            int doneScriptSize= doneScripts.size();
            for(Resource script : todoScripts){
                boolean move = false;

                //if(script.isDirectory()) move =true;
                if(!script.getFilename().endsWith(".sql"))move =true;
                if(move){
                    doneScripts.add(script.getFilename());
                    continue;
                }
                String fileName=script.getFilename().substring(0,script.getFilename().indexOf("."));
                String[] scriptIds = fileName.split("-");
                String scriptId = scriptIds[0];
                boolean requieredFulfilled = !wasExecuted(scriptIds[0]);
                if(requieredFulfilled)
                 {
                    for(int i=1;i<scriptIds.length;i++){
                        if(!wasExecuted(scriptIds[i])){
                            requieredFulfilled = false;
                            break;
                        }
                    }
                }else{
                    doneScripts.add(script.getFilename());
                    doneScriptIds.add(scriptId);
                }



                if(requieredFulfilled ){
                    executeFile( script);
                    jdbcTemplate.update("INSERT INTO `DB_VERSION` values ('"+scriptId+"','"+script.getFilename()+"',CURRENT_TIMESTAMP,NULL,NULL);");
                    doneScripts.add(script.getFilename());
                    doneScriptIds.add(scriptId);
                }
            }
            if(doneScripts.size()==doneScriptSize){
                throw new Exception("Cyclic SQL problem "+todoScripts.toString());
            }
        }
    }
    public static void createDataBaseIfInexistant(String drviverClass,String dbUrl,String dbUsername, String dbPassword,String dbName) throws SQLException {
        Connection conn = null;
        Statement stmt = null;
        try{
            //STEP 2: Register JDBC driver
            Class.forName(drviverClass);

            //STEP 3: Open a connection
            logger.info("Connecting to database...");
            conn = DriverManager.getConnection(dbUrl, dbUsername, dbPassword);

            //STEP 4: Execute a query
            logger.info("Creating database...");
            stmt = conn.createStatement();

            String sql = "CREATE DATABASE IF NOT EXISTS "+dbName;
            stmt.executeUpdate(sql);
            System.out.println("Database created successfully...");
        }catch(SQLException se){
            //Handle errors for JDBC
            se.printStackTrace();
        }catch(Exception e){
            //Handle errors for Class.forName
            e.printStackTrace();
        }finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                    stmt.close();
            }catch(SQLException se2){
            }// nothing we can do
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
            }//end finally try
        }//end try
        logger.info("Goodbye!");
    }
    private void executeFile(Resource ressource) throws SQLException {
        logger.info("Executin script :"+ ressource.getFilename());
        Connection connection = dataSource.getConnection();
        //Resource ressource = new InputStreamResource(getClass().getResourceAsStream(scriptPath));
        ScriptUtils.executeSqlScript(connection, ressource);
        connection.close();
    }
    private boolean wasExecuted(String version){
        int result =0;
        try{
            result = jdbcTemplate.queryForObject("SELECT COUNT(*) FROM DB_VERSION WHERE VERSION='"+version+"'", Integer.class);
        }catch (Exception e){
            logger.info("exception in query for script, its normal for V000  ;) ==  version : "+version,e);
            if(!"V000".equals(version))throw e;

        }
        return result != 0;
    }

    public void createDbVersion() throws Exception {
        logger.info("creating the  DB_VERSION...");
        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS DB_VERSION (VERSION varchar(50) NOT NULL, FILE_NAME varchar(100) NOT NULL, CREATION_DATE timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, UPDATING_DATE timestamp NULL DEFAULT NULL, DELETING_DATE timestamp NULL DEFAULT NULL, PRIMARY KEY (VERSION))");
        logger.info("DB_VERSION created ...");
    }

}
