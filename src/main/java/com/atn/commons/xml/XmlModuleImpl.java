package com.atn.commons.xml;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.ValidationEvent;
import javax.xml.bind.ValidationEventHandler;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;

/**
 * @author <a href="mailto:lotfi.rhouma@allence-tunisie.com">RHOUMA Lotfi</a>
 * @version $Id$
 * @since 1.0 - 22 aout 2013 - 15:34:06
 */
public class XmlModuleImpl implements XmlModuleFacade{

    private final static Log log = LogFactory.getLog(XmlModuleImpl.class);

    /**
     * The validation against the XSD schema should be implemented. The encoding used during marshalling must be UTF-8
     *
     * @param xmlDest   the destination Xml file.
     * @param xsdSchema the path to the schema - will be written in the xsi:noNamespaceSchemaLocation. It will be copied to the path of the resulting files to enable
     *                  validation against it.
     */
    public void marshall( final Object jaxbElement,  final File xmlDest,  final JAXBContext ctx,  final URL xsdSchema) {
        Marshaller m;
        try {
            String xsdFileName = this.getFileNameFrom(xsdSchema);
            m = ctx.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            m.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            m.setProperty(Marshaller.JAXB_NO_NAMESPACE_SCHEMA_LOCATION, xsdFileName);
            m.setSchema(this.getSchema(xsdSchema));
            //specifiy how to handle validation errors
            m.setEventHandler(this.getValidationEventHandler(XmlOperation.MARSHALLING));
            m.marshal(jaxbElement, xmlDest);
        } catch (JAXBException jaxbe) {
            throw new RuntimeException("There has been an unexpected exception during the XML marshalling process.", jaxbe);
        } catch (SAXException saxe) {
            throw new RuntimeException("SAX Error when trying to load the given '" + xsdSchema + "' XSD schema file", saxe);
        }
    }

    public void marshallAndCopySchema( final Object jaxbElement,  final File xmlDest,  final JAXBContext ctx,  final URL xsdSchema) {
        this.marshall(jaxbElement, xmlDest, ctx, xsdSchema);
        try {
            this.copyURLToPath(xmlDest.getParent(), xsdSchema);
        } catch (IOException ioe) {
            throw new RuntimeException("An error occured when copying the XSD schema file to '" + xmlDest.getParent() + '\'', ioe);
        }
    }

    /**
     * {@inheritDoc}
     */
    public <T> T unmarshall(Class<T> clazz,  final File xmlFile,  final JAXBContext ctx) {
        try {
            return this.unmarshallWithStax(new FileInputStream(xmlFile), ctx);
        } catch (FileNotFoundException fnfe) {
            throw new RuntimeException("Could not open file '" + xmlFile + '\'', fnfe);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override public <T> T unmarshall(final Class<T> clazz,  final InputStream stream,  final JAXBContext ctx) {
        return this.unmarshallWithStax(stream, ctx);
    }

    /**
     * Does the unmarshalling job. This implementation of unmarshall uses a StAX XMLEventReader. It doesn't validate the file to unmarshall against an Xschema
     *
     * @param xmlStream a stream representing an XML content in UTF-8
     * @param ctx       the JAXBContext
     *
     * @return the Object unmarshalled from the xml
     */
    @SuppressWarnings({"unchecked"})
    private <T> T unmarshallWithStax(InputStream xmlStream, JAXBContext ctx) {
        //Unmarshalling from a StAX XMLEventReader:
        Unmarshaller um = null;
        try {
            um = ctx.createUnmarshaller();
            //
            //BufferedReader is used for more efficiency reading bytes.
            //InputStreamReader is used to read bytes with a specified encoding
            //FileInputStream transforms a File into a Stream of bytes
            BufferedReader in = null;
            in = new BufferedReader(new InputStreamReader(xmlStream, "UTF-8"));
            //
            XMLInputFactory xmlIF = XMLInputFactory.newInstance();
            XMLEventReader xmlER = xmlIF.createXMLEventReader(in);
            //specify the Schema to use for validation - enable validation
            //um.setSchema();
            //specifiy how to handle validation errors
            um.setEventHandler(this.getValidationEventHandler(XmlOperation.UNMARSHALLING));
            //unmarshall
            T t = (T) um.unmarshal(xmlER);
            //close the BufferReader
            in.close();
            return t;
        } catch (JAXBException jaxbe) {
            throw new RuntimeException("Problem occured when trying to create an unmarshaller", jaxbe);
        } catch (UnsupportedEncodingException uee) {
            throw new RuntimeException("Problem occured when trying to create an unmarshaller, setting the event handler or uduring nmarshalling", uee);
        } catch (XMLStreamException xse) {
            throw new RuntimeException("Unknown error occured during creation of the XMLEventHandler", xse);
        } catch (IOException ioe) {
            throw new RuntimeException("Problem occured when trying to close the BufferReader", ioe);
        }
    }

    /**
     * Allows copy of the orignal validation Schema to be copied along with the marshalled file.
     * <p/>
     * NB: Do not take in account external XML included file as in <xs:include schemaLocation="demat_base.xsd"/>
     * <p/>
     * todo: transfer this Util method to a common URLUtil class
     *
     * @param path the <Code>String</Code> representing the path to copy the file to
     * @param url  the <Code>URL</Code> of the resource to be copied
     *
     * @throws FileNotFoundException if the file exists but is a directory rather than a regular file, does not exist but cannot be created, or cannot be opened for
     *                               any other reason
     * @throws IOException           If the first byte cannot be read for any reason other than the end of the file, if the input stream has been closed, or if some
     *                               other I/O error occurs.
     */
    private void copyURLToPath( final String path,  final URL url) throws IOException {
        String xsdFileName = this.getFileNameFrom(url);
        InputStream in = url.openStream();
        FileOutputStream out = new FileOutputStream(new File(path + File.separator + xsdFileName));
        //
        final int BUF_SIZE = 1 << 8;//bit shift: @see http://java.sun.com/docs/books/tutorial/java/nutsandbolts/op3.html
        byte[] buffer = new byte[BUF_SIZE];
        int bytesRead = -1;
        while ((bytesRead = in.read(buffer)) > -1) {
            out.write(buffer, 0, bytesRead);
        }
        in.close();
        out.close();
    }

    /**
     * return the file name only from a given URL
     *
     * @param url a <Code>URL</Code> representing a path to a file
     *
     * @return the file name only, the String after the last separator /
     */
    private String getFileNameFrom(URL url) {
        return url.getFile().substring(url.getFile().lastIndexOf('/') + 1);
    }

    /**
     * return a ValidationEventHandler object in a consistent manner. In other words, defines a way to handle validation errors.
     * <p/>
     * By using this method in validation action, you ensure a consistent way of handling errors.
     * <p/>
     * Currently in case of any event, the process is stopped by having handleEvent methode returning false
     *
     * @param operation the type of XmlOperation
     *
     * @return a valid, unified, ValidationEventHandler
     */
    private ValidationEventHandler getValidationEventHandler( final XmlOperation operation) {
        return new ValidationEventHandler() {
            public boolean handleEvent(ValidationEvent event) {
                log.error("An error occured during the " + operation.name() + "operation. Details follow:\n" + event);
                return false;
            }
        };
    }

    /**
     * Defines the possible Xml operation types
     */
    private enum XmlOperation {
        MARSHALLING, UNMARSHALLING;
    }

    /**
     * Returns a <Code>Shema</Code> object from a given <Code>File</Code> object representing the physical XSD file.
     *
     * @param schema <Code>URL</Code> representing the XS
     *
     * @return a <Code>Shema</Code>
     *
     * @throws SAXException If a SAX error occurs during parsing.
     */
    private Schema getSchema( final URL schema) throws SAXException {
        SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        return factory.newSchema(schema);
    }
}
