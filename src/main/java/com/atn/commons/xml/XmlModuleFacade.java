package com.atn.commons.xml;

import java.io.File;
import java.io.InputStream;
import java.net.URL;

import javax.xml.bind.JAXBContext;


/**
 * @author <a href="mailto:lotfi.rhouma@allence-tunisie.com">RHOUMA Lotfi</a>
 * @version $Id$
 * @since 1.0 - 22 aout 2013 - 15:32:44
 */
public interface XmlModuleFacade {

    /**
     * Marshall (transform Java object- jaxbElement - into XML) the given Object (which must have corresponding JAXB bindings) into the given File.
     *
     * @param jaxbElement the Object instance to marshall to XML
     * @param xmlDest     the destination File to receive the generated XML
     * @param ctx         the JAXBContext instance to use - defines the jabxElement (marshallables)
     * @param xsdSchema   the XSD shema that must be enforced by the generated XML file
     */
    void marshall(Object jaxbElement, File xmlDest, JAXBContext ctx, URL xsdSchema);

    /**
     * Same as above but specified XSD schema will be copied along with the marshalled file.
     */
    void marshallAndCopySchema(Object jaxbElement, File xmlDest, JAXBContext ctx, URL xsdSchema);

    /**
     * Unmarshall an XML structure into a Java object
     *
     * @param clazz   the type of the resulting Object
     * @param xmlFile the source XML file to be unmarshalled
     * @param ctx     the JAXBContext, the bindings which define how to unmarshall
     *
     * @return An unmarshaleld T object
     */
    <T> T unmarshall(Class<T> clazz, File xmlFile, JAXBContext ctx);

    /**
     * Same as above but takes an InputStream as input instead of a File.
     */
    <T> T unmarshall(Class<T> clazz, InputStream stream, JAXBContext ctx);
}
