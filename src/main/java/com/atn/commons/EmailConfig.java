package com.atn.commons;

import com.atn.commons.email.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

@Configuration
@PropertySource(value = {"classpath:config.properties"}, ignoreResourceNotFound = false)
public class EmailConfig {
    private static Logger logger = LogManager.getLogger(EmailConfig.class);
    private static Properties configuration = CommonsConfig.configuration();

    @Bean
    public EmailHandler emailHandler() throws Exception {
        EmailHandler emailHandler = null;
        Properties mailConfiguration = CommonsConfig.configuration();
        String mailProvidersValue = mailConfiguration.getProperty("mail.providers");
        if(mailProvidersValue.indexOf("file:")==0){
            String filePath = mailProvidersValue.substring(5);
            mailConfiguration = new Properties();
            mailConfiguration.load(new FileInputStream(filePath));
            mailProvidersValue = mailConfiguration.getProperty("mail.providers");
        }else if(mailProvidersValue.indexOf("classpath:")==0){
            String filePath = mailProvidersValue.substring(10);
            mailConfiguration = new Properties();
            mailConfiguration.load(CommonsConfig.class.getClassLoader().getResourceAsStream(filePath));
            mailProvidersValue = mailConfiguration.getProperty("mail.providers");
        }
        String[] mailProviders = mailProvidersValue.split(",");

        for (int i = 0; i < mailProviders.length; i++) {
            EmailHandler currentEmailHandler = null;
            String providerName = mailProviders[i];
            String providerType = mailConfiguration.getProperty(providerName + ".type");
            if ("LogMailHandler".equals(providerType)) {
                currentEmailHandler = new LogMailHandler();
            } else if ("GmailSenderEngineHandler".equals(providerType)) {
                String username = mailConfiguration.getProperty(providerName + ".username");
                String password = mailConfiguration.getProperty(providerName + ".password");
                String to = mailConfiguration.getProperty(providerName + ".to");
                boolean disabled = Boolean.parseBoolean(mailConfiguration.getProperty(providerName + ".disabled"));
                String from = mailConfiguration.getProperty(providerName + ".from");
                currentEmailHandler = new GmailSenderEngineHandler(username, password, to, disabled, from);
            } else if ("FileSenderEngineHandler".equals(providerType)) {
                String rootPath = mailConfiguration.getProperty(providerName + ".rootpath");
                String to = mailConfiguration.getProperty(providerName + ".to");
                boolean disabled = Boolean.parseBoolean(mailConfiguration.getProperty(providerName + ".disabled"));
                String from = mailConfiguration.getProperty(providerName + ".from");
                currentEmailHandler = new FileSenderEngineHandler(rootPath, to, disabled, from);
            } else if ("SendinBlueSenderEngineHandler".equals(providerType)) {
                String username = mailConfiguration.getProperty(providerName + ".username");
                String password = mailConfiguration.getProperty(providerName + ".password");
                String to = mailConfiguration.getProperty(providerName + ".to");
                boolean disabled = Boolean.parseBoolean(mailConfiguration.getProperty(providerName + ".disabled"));
                String from = mailConfiguration.getProperty(providerName + ".from");
                currentEmailHandler = new GmailSenderEngineHandler(username, password, to, disabled, from);
            } else if ("MandrillSenderEngineHandler".equals(providerType)) {
                String username = mailConfiguration.getProperty(providerName + ".username");
                String password = mailConfiguration.getProperty(providerName + ".password");
                String to = mailConfiguration.getProperty(providerName + ".to");
                boolean disabled = Boolean.parseBoolean(mailConfiguration.getProperty(providerName + ".disabled"));
                String from = mailConfiguration.getProperty(providerName + ".from");
                currentEmailHandler = new MandrillSenderEngineHandler(username, password, to, disabled, from);
            } else if ("AmazonAWSSenderMailHandler".equals(providerType)) {
                String host = mailConfiguration.getProperty(providerName + ".host");
                String username = mailConfiguration.getProperty(providerName + ".username");
                String password = mailConfiguration.getProperty(providerName + ".password");
                String to = mailConfiguration.getProperty(providerName + ".to");
                boolean disabled = Boolean.parseBoolean(mailConfiguration.getProperty(providerName + ".disabled"));
                String from = mailConfiguration.getProperty(providerName + ".from");
                currentEmailHandler = new AmazonAWSSenderMailHandler(host,username, password, to, disabled, from);
            }else if ("ZohoSenderEngineHandler".equals(providerType)) {
                String username = mailConfiguration.getProperty(providerName + ".username");
                String password = mailConfiguration.getProperty(providerName + ".password");
                String to = mailConfiguration.getProperty(providerName + ".to");
                boolean disabled = Boolean.parseBoolean(mailConfiguration.getProperty(providerName + ".disabled"));
                String from = mailConfiguration.getProperty(providerName + ".from");
                currentEmailHandler = new ZohoSenderEngineHandler(username, password, to, disabled, from);
            }else{
                throw new Exception(providerType+" : this provider is not managed ");
            }

            if (currentEmailHandler != null) {
                if (emailHandler == null) {
                    emailHandler = currentEmailHandler;
                } else {
                    emailHandler.setSuccessor(currentEmailHandler);
                }
            }
        }
        return emailHandler;
    }


}
