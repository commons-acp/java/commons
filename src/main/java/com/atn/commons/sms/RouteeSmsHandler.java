package com.atn.commons.sms;

import com.atn.commons.sms.exceptions.*;
import com.atn.commons.sms.responses.ErrorResponse;
import com.atn.commons.sms.responses.RouteeApiResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.*;

public class RouteeSmsHandler extends AbstractSmsHandler {

    private String from;
    private String applicationId;
    private String applicationSecret;
    private static Logger logger = LogManager.getLogger(RouteeSmsHandler.class);

    public RouteeSmsHandler(String applicationId, String applicationSecret, String from, boolean active) {
        super(active, from);
        this.applicationId = applicationId;
        this.applicationSecret = applicationSecret;
        this.from = from;

    }


    /**
     * @author Monta
     * <p>this methode allows you to send an sms message using Routee</p>
     * @param message <code>String</code> message to be sent
     * @param number <code>String</code> number to send to in +(Code) 0000 format
     * @throws SmsSendingFailure  in case the sending fails
     * @throws IOException
     */
    @Override
    protected void sendSms(String message, String number) throws SmsSendingFailure, IOException {
        String token = null;
        try {
            token = getCredentials().getAccess_token();
        } catch (InvalidRouteeCredential e){
            logger.error(e);
        }

        OkHttpClient client = new OkHttpClient();
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, "{ \"body\": \"" + message + "\",\"to\" : \"" + number + "\",\"from\": \" " + from + "\"}");
        Request request = new Request.Builder()
                .url("https://connect.routee.net/sms")
                .post(body)
                .addHeader("authorization", "Bearer " + token)
                .addHeader("content-type", "application/json")
                .build();

        Response response = client.newCall(request).execute();
        ObjectMapper objectMapper = new ObjectMapper();
        if (response.code() != 200){
            ErrorResponse errorResponse = objectMapper.readValue(response.body().string(), ErrorResponse.class);
            throw new SmsSendingFailure(errorResponse.getDescription(),errorResponse.getCode(),response.code());
        }

    }

    /**
     * @param decoded <code>String</code>
     * @return <code>String</code> Base64 coded String
     * @author Monta
     * @since v3.77
     */
    private String encodeBase64(String decoded) throws IOException {
        return Base64.getEncoder()
                .encodeToString(decoded.getBytes());
    }

    /**
     * @return <code>RouteeApiResponse</code>  response
     * @throws InvalidRouteeCredential if this exception is thrown that means you entered a bad credentials
     * @throws IOException             this is thrown because of invalid encoding
     * @author Monta
     * <p> this methode allow you to get a token from the routee server based on your appId and appSecret</p>
     * @see RouteeApiResponse
     */
    private RouteeApiResponse getCredentials() throws InvalidRouteeCredential, IOException {
        OkHttpClient client = new OkHttpClient();
        String credentials = this.applicationId + ":" + this.applicationSecret;
        MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
        RequestBody body = RequestBody.create(mediaType, "grant_type=client_credentials");
        Request request = new Request.Builder()
                .url("https://auth.routee.net/oauth/token")
                .post(body)
                .addHeader("authorization", "Basic " + encodeBase64(credentials))
                .addHeader("content-type", "application/x-www-form-urlencoded")
                .build();
        Response response = client.newCall(request).execute();
        ObjectMapper objectMapper = new ObjectMapper();
        if (response.code() == 200) {
            return objectMapper.readValue(response.body().string(), RouteeApiResponse.class);
        } else {
            throw new InvalidRouteeCredential("your sms configuration (username or password) is invalid , you entred a bad credentiels", 000400l,response.code());
        }

    }


}
