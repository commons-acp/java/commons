package com.atn.commons.sms;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

public class LogSmsHandler extends AbstractSmsHandler {

    public LogSmsHandler() {
        super(true,"logger");
    }

    private static Logger logger = LogManager.getLogger(LogSmsHandler.class);

    @Override
    protected void sendSms(String message, String number) throws IOException {

        logger.info("#######################-SMS-############################");
        logger.info("#######################-Receiver-############################");
        logger.info("to : " + number);
        logger.info("#########################-Message-#############################");
        logger.info("message : " + message);
        logger.info("######################################################");
        }

}
