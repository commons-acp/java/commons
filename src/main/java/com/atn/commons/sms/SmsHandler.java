package com.atn.commons.sms;

import java.io.IOException;

public interface SmsHandler {
    void send(String subject, String number) throws IOException;
    void setNextProvider(SmsHandler smsHandler);
}
