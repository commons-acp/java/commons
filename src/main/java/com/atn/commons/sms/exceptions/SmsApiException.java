package com.atn.commons.sms.exceptions;

import lombok.Data;

public abstract class SmsApiException extends Exception{

    public SmsApiException(final String message) {
        super(message);
    }

    public ErrorResponse getError(){
        return new ErrorResponse(getStatus(),getCode(),getMessage());
    }

    protected abstract Long getCode();
    protected abstract int getStatus();


    @Data
    public static class ErrorResponse {
        private final int status;
        private final Long code;
        private final String message;
    }
}
