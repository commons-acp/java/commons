package com.atn.commons.sms.exceptions;

public class InvalidRouteeCredential extends SmsApiException {

    private final Long code;
    private final int status;

    public InvalidRouteeCredential(String message , Long code , int status) {
        super(message);
        this.code = code;
        this.status = status;
    }

    @Override
    protected Long getCode() {
        return this.code;
    }

    @Override
    protected int getStatus() {
        return this.status;
    }

}
