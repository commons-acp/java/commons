package com.atn.commons.sms;


import com.atn.commons.sms.exceptions.InvalidRouteeCredential;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

public abstract class AbstractSmsHandler implements SmsHandler {

    private SmsHandler successor;
    private boolean active = false;
    protected String from = "allence-tunisie";
    private static Logger logger = LogManager.getLogger(AbstractSmsHandler.class);


    public AbstractSmsHandler(boolean active, String from) {
        this.active = active;
        this.from = from;
    }

    protected abstract void sendSms(String message, String number) throws Exception;

    public void send(String message, String number) {
        if (!active) {
            sendtoNextProvider(message, from);
        } else {
            try {
                sendSms(message,number);
            } catch (Exception e) {
                logger.error(e);
                sendtoNextProvider(message,number);
            }
        }
    }

    public void setNextProvider(SmsHandler smsHandler) {
        if(this.successor == null){
            this.successor = smsHandler;
        }else{
            this.successor.setNextProvider(successor);
        }

    }
    protected final void sendtoNextProvider(String message, String from)  {
        if (successor != null) {
            try {
                successor.send(message,from);
            } catch (IOException e) {
                logger.error(e.getMessage());
            }
        }
    }




}
