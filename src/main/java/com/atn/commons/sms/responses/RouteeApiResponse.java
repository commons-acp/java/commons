package com.atn.commons.sms.responses;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RouteeApiResponse {
    private String access_token;
    private String token_type;
    private Long expires_in;
    private String scope;
    private List<String> permissions;
}
