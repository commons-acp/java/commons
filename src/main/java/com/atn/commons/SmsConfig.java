package com.atn.commons;


import com.atn.commons.sms.LogSmsHandler;
import com.atn.commons.sms.RouteeSmsHandler;
import com.atn.commons.sms.SmsHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.io.FileInputStream;
import java.util.Properties;

@Configuration
@PropertySource(value = {"classpath:config.properties"}, ignoreResourceNotFound = false)
public class SmsConfig {

    private static Logger logger = LogManager.getLogger(SmsConfig.class);
    private static Properties configuration = CommonsConfig.configuration();

    @Bean
    public SmsHandler smsHandler() throws Exception {
        SmsHandler smsHandler = null;
        Properties smsConfiguration = CommonsConfig.configuration();
        String smsProvidersValue = smsConfiguration.getProperty("sms.providers");
        if (smsProvidersValue.indexOf("file:") == 0) {
            String file = smsProvidersValue.substring(5);
            String value = "null";
            smsConfiguration = new Properties();
            smsConfiguration.load(new FileInputStream(file));
            smsProvidersValue = smsConfiguration.getProperty("sms.providers");
        }else if (smsProvidersValue.indexOf("classpath:") == 0) {
            String filePath = smsProvidersValue.substring(10);
            smsConfiguration = new Properties();
            smsConfiguration.load(CommonsConfig.class.getClassLoader().getResourceAsStream(filePath));
            smsProvidersValue = smsConfiguration.getProperty("sms.providers");
        }
        String[] smsProviders = smsProvidersValue.split(",");

        for (int i = 0; i < smsProviders.length; i++) {
            SmsHandler handler = null;
            String smsProviderType = smsProviders[i];
            String smsProviderName = smsConfiguration.getProperty(smsProviderType + ".name");
            if ("LogSmsHandler".equals(smsProviderName)) {
                handler = new LogSmsHandler();
            } else if("RouteeSmsHandler".equals(smsProviderName)){
                String applicationId = smsConfiguration.getProperty(smsProviderType+".id");
                String applicationSecret = smsConfiguration.getProperty(smsProviderType+".secret");
                String from = smsConfiguration.getProperty(smsProviderType+".sender");
                boolean active = Boolean.parseBoolean(smsConfiguration.getProperty(smsProviderType + ".active"));

                handler = new RouteeSmsHandler(applicationId, applicationSecret, from , active);
            }else{
                throw new Exception(smsProviderName+" : this provider is not managed ");
            }
            if (handler != null) {
                if (smsHandler == null) {
                    smsHandler = handler;
                } else {
                    smsHandler.setNextProvider(handler);
                }
            }
        }
        return smsHandler;
        }

}
