package com.atn.commons.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Properties;

public class PropertiesManager {
	public static List<String> notInDestinationFieldList(InputStream source,
			InputStream destination) throws IOException {
		Properties properties1 = createProperties(source);
		Properties properties2 = createProperties(destination);
		List<String> result = new ArrayList<String>();
		for (Entry<Object, Object> entry : properties1.entrySet()) {
			Object key = entry.getKey();
			Object value = properties2.get(key);
			if (value == null) {
				result.add(key.toString());
			}
		}
		return result;
	}

	public static Properties createProperties(InputStream inStream)
			throws IOException {
		Properties properties = new Properties();
		properties.load(inStream);
		return properties;
	}
}
