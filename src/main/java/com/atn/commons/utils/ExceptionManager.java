package com.atn.commons.utils;

import java.io.PrintWriter;
import java.io.StringWriter;

public class ExceptionManager {
	public static String toHtml(Throwable t) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		t.printStackTrace(pw);
		String stackTrace = sw.toString();
		return stackTrace.replace(System.getProperty("line.separator"), "<br/>\n");
	}
}
