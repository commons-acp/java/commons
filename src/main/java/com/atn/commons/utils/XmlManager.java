package com.atn.commons.utils;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public final class XmlManager {
	public static<T> String serialize(Class<T> clazz,T o) throws JAXBException{
		StringWriter writer = new StringWriter();
		JAXBContext context = JAXBContext.newInstance(clazz);
		Marshaller m = context.createMarshaller();
		m.marshal(o, writer);
		return writer.toString();
	}
	@SuppressWarnings("unchecked")
	public static<T> T deserialize(Class<T> clazz,String input) throws JAXBException{
		JAXBContext context = JAXBContext.newInstance(clazz);
		Unmarshaller m = context.createUnmarshaller();
		return (T)m.unmarshal(new StringReader(input));
	}
}
