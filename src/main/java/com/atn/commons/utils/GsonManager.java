package com.atn.commons.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class GsonManager {
    public static String write(Object object){
        final GsonBuilder builder = new GsonBuilder();
        final Gson gson = builder.create();
        return gson.toJson(object);
    }
    public static <T extends Object> T read(Class<T> clazz, String json){
        final GsonBuilder builder = new GsonBuilder();
        final Gson gson = builder.create();
        return gson.fromJson(json,clazz);
    }
}
