package com.atn.commons.utils;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.apache.pdfbox.multipdf.Splitter;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;

import java.io.*;
import java.util.ArrayList;
import java.util.List;


public class PdfManager {
    public static void writeFromHtml(OutputStream out, StringBuilder htmlString) throws DocumentException, IOException {
        com.itextpdf.text.Document document = new com.itextpdf.text.Document();
        PdfWriter writer = PdfWriter.getInstance(document, out);

        document.open();
        InputStream is = new ByteArrayInputStream(htmlString.toString().getBytes());
        XMLWorkerHelper.getInstance().parseXHtml(writer, document, is);
        document.close();
        out.close();
    }

    /**
     * To use this method you need to add to class path a dll for GS
     * wind gsll.dll
     * win64 gsdll64.dll
     *
     * @param pdfFile
     * @return
     * @throws FileNotFoundException
     * @throws IOException
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static List<java.awt.Image> split(File pdfFile)
            throws FileNotFoundException, IOException {
        List<java.awt.Image> result = new ArrayList<>();
        PDDocument document = PDDocument.load(pdfFile);
        Splitter splitter = new Splitter();
        List<PDDocument> pages = splitter.split(document);
        for (int i = 0; i < pages.size(); i++) {
            PDDocument pdDocument = pages.get(i);
            PDFRenderer pdfRenderer = new PDFRenderer(pdDocument);
            result.add(pdfRenderer.renderImageWithDPI(0, 300, ImageType.RGB));
            pdDocument.close();
        }
        document.close();
        return result;
    }

    public static int getNumberPages(String path) throws IOException {
        PDDocument doc = PDDocument.load(new File(path));
        int count = doc.getNumberOfPages();
        return count;
    }

    public static void exportPage(File pdfFile, int pageNumber, File outPutFile) throws IOException {
        PDDocument document = PDDocument.load(pdfFile);
        Splitter splitter = new Splitter();
        List<PDDocument> pages = splitter.split(document);
        PDDocument pdDocument = pages.get(pageNumber);
        pdDocument.save(outPutFile);
    }

    public static void exportPage(File pdfFile, int from, int to, File outPutFile) throws IOException {
        PDDocument document = PDDocument.load(pdfFile);
        Splitter splitter = new Splitter();
        List<PDDocument> pages = splitter.split(document);
        PDDocument pdDocument = pages.get(from);
        for (int i = from + 1; i <= to; i++) {
            pdDocument.addPage(pages.get(i).getPage(0));
        }
        pdDocument.save(outPutFile);
    }

    public static String readText(File pdfFile, int pageNumber) throws IOException {
        String content = "";
        PdfReader reader = new PdfReader(new FileInputStream(pdfFile));
        int n = reader.getNumberOfPages();
        content = PdfTextExtractor.getTextFromPage(reader, pageNumber); //Extracting the content from a particular page.
        reader.close();

        return content;
    }

    public static File fusion(File pdf1, File pdf2) throws IOException {
        PDDocument doc1 = PDDocument.load(pdf1);
        PDDocument doc2 = PDDocument.load(pdf2);
        PDFMergerUtility PDFmerger = new PDFMergerUtility();
        PDFmerger.setDestinationFileName(pdf1.getAbsolutePath());
        PDFmerger.addSource(pdf1);
        PDFmerger.addSource(pdf2);
        PDFmerger.mergeDocuments();
        System.out.println("Documents merged");
        doc1.close();
        doc2.close();
        return new File(pdf1.getAbsolutePath());
    }

    public static File image2pdf(File image) throws IOException {
        File pdf = new File(image.getAbsolutePath()+".pdf");
        pdf.createNewFile();
        PDDocument document = new PDDocument();
        document.addPage(new PDPage());
        document.save(pdf.getAbsolutePath());
        PDPage page = document.getPage(0);
        PDImageXObject pdImage = PDImageXObject.createFromFile(image.getAbsolutePath(), document);
        PDPageContentStream contentStream = new PDPageContentStream(document, page);
        contentStream.drawImage(pdImage, 70, 250);
        contentStream.close();
        document.save(pdf.getAbsolutePath());
        document.close();
        return pdf;
    }
}
