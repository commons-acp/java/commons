package com.atn.commons.utils;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.imageio.ImageIO;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.ChecksumException;
import com.google.zxing.DecodeHintType;
import com.google.zxing.EncodeHintType;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.RGBLuminanceSource;
import com.google.zxing.Result;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageConfig;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.QRCodeReader;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

public class BarCodeManager {
	/**
	 * public static void main(String[] args) throws WriterException,
	 * IOException { String qrCodeText = "Hello Mutuaide"; String filePath =
	 * "D:\\tmp\\QRCODE_" + new Date().getTime() + ".png"; File logoFile = new
	 * File("D:\\tmp\\headshot.png"); int size = 100; FileInputStream fis =
	 * null; do { size += 100; FileOutputStream fos = new
	 * FileOutputStream(filePath); writeQrCode(fos, qrCodeText, size,
	 * 0xFFFFFFFF,0xFF000000,logoFile); fis = new FileInputStream(new
	 * File(filePath)); } while (!checkQrCode(qrCodeText, fis)); fis.close();
	 * System.out.println("DONE");
	 * 
	 * }
	 */
	/**
	 * 
	 * @param excepted
	 * @param fis
	 * @return
	 * @throws IOException
	 */
	public static boolean checkQrCode(String excepted, InputStream fis) throws IOException {
		try {
			String result = readQrCode(fis);
			if (result.equals(excepted))
				return true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			fis.close();
		}
		return false;
	}
	public static void writeQrCode(OutputStream outputStream, String qrCodeText, int size, int backGroudColor,
			int frontColor, File logoIs) throws WriterException, IOException {
		writeQrCode(outputStream, qrCodeText, size, backGroudColor, frontColor, logoIs, 4);
	}
	/**
	 * 
	 * @param outputStream
	 * @param qrCodeText
	 * @param size
	 * @param backGroudColor
	 * @param frontColor
	 * @param logoIs
	 * @throws WriterException
	 * @throws IOException
	 */
	public static void writeQrCode(OutputStream outputStream, String qrCodeText, int size, int backGroudColor,
			int frontColor, File logoIs,int margin) throws WriterException, IOException {

		// Create new configuration that specifies the error correction
		Map<EncodeHintType, Object> hints = new HashMap<>();
		hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
		hints.put(EncodeHintType.MARGIN, margin);
		QRCodeWriter writer = new QRCodeWriter();
		BitMatrix bitMatrix = null;
		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		try {
			// Create a qr code with the url as content and a size of 250x250 px
			bitMatrix = writer.encode(qrCodeText, BarcodeFormat.QR_CODE, size, size, hints);

			MatrixToImageConfig config = new MatrixToImageConfig(frontColor, backGroudColor);

			// Load QR image
			BufferedImage qrImage = MatrixToImageWriter.toBufferedImage(bitMatrix, config);

			if (logoIs != null) {
				// Load logo image
				BufferedImage logoImage = ImageIO.read(logoIs);

				// Calculate the delta height and width between QR code and logo
				int deltaHeight = qrImage.getHeight() - logoImage.getHeight();
				int deltaWidth = qrImage.getWidth() - logoImage.getWidth();

				// Initialize combined image
				BufferedImage combined = new BufferedImage(qrImage.getHeight(), qrImage.getWidth(),
						BufferedImage.TYPE_INT_ARGB);
				Graphics2D g = (Graphics2D) combined.getGraphics();

				// Write QR code to new image at position 0/0
				g.drawImage(qrImage, 0, 0, null);
				g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1f));

				// Write logo into combine image at position (deltaWidth / 2)
				// and
				// (deltaHeight / 2). Background: Left/Right and Top/Bottom must
				// be
				// the same space for the logo to be centered
				g.drawImage(logoImage, (int) Math.round(deltaWidth / 2), (int) Math.round(deltaHeight / 2), null);

				// Write combined image as PNG to OutputStream
				ImageIO.write(combined, "png", baos);
			} else {
				ImageIO.write(qrImage, "png", baos);
			}
		} catch (WriterException e) {
			// LOG.error("WriterException occured", e);
		} catch (IOException e) {
			// LOG.error("IOException occured", e);
		}

		// Do something with the OutputStream
		baos.writeTo(outputStream);
	}

	/**
	 * 
	 * @param inputStream
	 * @return
	 * @throws NotFoundException
	 * @throws ChecksumException
	 * @throws FormatException
	 * @throws IOException
	 */
	public static String readQrCode(InputStream inputStream)
			throws NotFoundException, ChecksumException, FormatException, IOException {
		BufferedImage image = ImageIO.read(inputStream);
		BinaryBitmap bitmap = null;
		Result result = null;

		int[] pixels = image.getRGB(0, 0, image.getWidth(), image.getHeight(), null, 0, image.getWidth());
		RGBLuminanceSource source = new RGBLuminanceSource(image.getWidth(), image.getHeight(), pixels);

		try {
			bitmap = new BinaryBitmap(new HybridBinarizer(source));
		} catch (IllegalArgumentException e) {
			if (bitmap == null) {
				return null;
			}
		}
		Hashtable<DecodeHintType, Object> hintMap = new Hashtable<DecodeHintType, Object>();
		hintMap.put(DecodeHintType.TRY_HARDER, Boolean.TRUE);
		QRCodeReader reader = new QRCodeReader();
		result = reader.decode(bitmap, hintMap);
		return result.getText();

	}
}
