package com.atn.commons.utils;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.springframework.core.io.Resource;

import java.io.*;
import java.util.Map;

public class HtmlManager {

    private static String changeValue(InputStream file, Map<String, String> mapDataChange) throws FileNotFoundException {
        Velocity.init();

        VelocityContext context = new VelocityContext();

        for(Map.Entry<String, String> entry : mapDataChange.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            context.put(key, value);

        }
        //Evaluate
        StringWriter swOut = new StringWriter();
        Velocity.evaluate(context, swOut, "test", file);

        String result =  swOut.getBuffer().toString();
        System.out.println(result);
        return result;
    }


    public static void writeUsingOutputStream(InputStream inputFile, Map<String, String> mapDataChange,OutputStream outputStream) throws IOException {



        String data =changeValue(inputFile,  mapDataChange);
        try {
            outputStream.write(data.getBytes(), 0, data.length());
        } catch (IOException e) {
            throw e;
        }finally{
                outputStream.close();
        }

    }



}
