package com.atn.commons.utils;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZipManager {

    /**
     *
      * @param srcFiles<Name,Path></Name,Path>
     * @param outputStream
     */
    public static  void zip(Map<String,String> srcFiles, OutputStream outputStream) {


        try {

            // create byte buffer
            byte[] buffer = new byte[1024];

            ZipOutputStream
                    zos = new ZipOutputStream(outputStream);

            for(Map.Entry<String,String> srcFilePath : srcFiles.entrySet()){
                File srcFile = new File(srcFilePath.getValue());
                if(!srcFile.exists())continue;
                FileInputStream fis = new FileInputStream(srcFile);

                // begin writing a new ZIP entry, positions the stream to the start of the entry data
                zos.putNextEntry(new ZipEntry(srcFilePath.getKey()+ "."+FilenameUtils.getExtension(srcFilePath.getValue())));

                int length;

                while ((length = fis.read(buffer)) > 0) {
                    zos.write(buffer, 0, length);
                }

                zos.closeEntry();

                // close the InputStream
                fis.close();

            }

            // close the ZipOutputStream
            zos.close();

        }
        catch (IOException ioe) {
            System.out.println("Error creating zip file: " + ioe);
        }

    }

    /**
     * This method zips the directory
     * @param dir
     * @param zipDirName
     */
    public static void zipDirectory(File dir, String zipDirName) throws FileNotFoundException {
        	List<String> filesListInDir = populateFilesList(dir);
            //now zip files one by one
            //create ZipOutputStream to write to the zip file
            FileOutputStream fos = new FileOutputStream(zipDirName);

    }
    /**
     * This method populates all the files in a directory to a List
     * @param dir
     * @throws IOException
     */
    private static List<String> populateFilesList(File dir) {
    	List<String> filesListInDir = new ArrayList<>();
        File[] files = dir.listFiles();
        for(File file : files){
            if(file.isFile()) filesListInDir.add(file.getAbsolutePath());
            else filesListInDir.addAll(populateFilesList(file));
        }
        return filesListInDir;
    }

    /**
     * This method compresses the single file to zip format
     * @param file
     * @param zipFileName
     */
    public static void zipSingleFile(File file, String zipFileName) {
        try {
            //create ZipOutputStream to write to the zip file
            FileOutputStream fos = new FileOutputStream(zipFileName);
            ZipOutputStream zos = new ZipOutputStream(fos);
            //add a new Zip Entry to the ZipOutputStream
            ZipEntry ze = new ZipEntry(file.getName());
            zos.putNextEntry(ze);
            //read the file and write to ZipOutputStream
            FileInputStream fis = new FileInputStream(file);
            byte[] buffer = new byte[1024];
            int len;
            while ((len = fis.read(buffer)) > 0) {
                zos.write(buffer, 0, len);
            }
            
            //Close the zip entry to write to zip file
            zos.closeEntry();
            //Close resources
            zos.close();
            fis.close();
            fos.close();
            System.out.println(file.getCanonicalPath()+" is zipped to "+zipFileName);
            
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


}
