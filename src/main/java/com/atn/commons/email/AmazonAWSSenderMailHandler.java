package com.atn.commons.email;

// Import log4j classes.
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import javax.mail.*;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.util.Map;
import java.util.Properties;

public class AmazonAWSSenderMailHandler extends AbstractSenderMailHandler {
    private String host = "email-smtp.eu-west-1.amazonaws.com";
    private static Logger logger = LogManager.getLogger(AmazonAWSSenderMailHandler.class);
    private String username;
    private String password;
    private Properties properties = new Properties();

    public AmazonAWSSenderMailHandler(String host,String username, String password, String to, boolean disabled, String from) {
        super(to, disabled, from);
        this.username = username;
        this.password = password;
        this.host = host;
        properties = System.getProperties();
        properties.put("mail.transport.protocol", "smtp");
        properties.put("mail.smtp.port", "465");
        properties.put("mail.smtp.ssl.enable", "true");
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.ssl.trust", "*");
    }

    @Override
    protected void sendHtmlEmail(String tos,String cc,String cci, String object, String htmlMessage, Map<File,String> fileMap) throws Exception {
        // Create a Session object to represent a mail session with the specified properties.
        Session session = Session.getDefaultInstance(properties);
        Transport transport = null;

        transport = session.getTransport();

        final MimeMessage message = getMimeMessage(session,tos,cc,cci,object,htmlMessage,fileMap);
        logger.info("Sending...");


        // Connect to Amazon SES using the SMTP username and password you specified above.
        transport.connect(host, username, password);

        // Send the email.
        transport.sendMessage(message, message.getAllRecipients());
        logger.info("Email sent!");
    }
}
