package com.atn.commons.email;

import java.io.File;
import java.util.Map;
import java.util.Properties;

import javax.activation.DataSource;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class ZohoSenderEngineHandler extends AbstractSenderMailHandler {
    private static Logger logger = LogManager.getLogger(ZohoSenderEngineHandler.class);
    private String username;
    private String password;
    private Properties properties = new Properties();

    public ZohoSenderEngineHandler(String username,
                                   String password,String to, boolean disabled, String from) {
        super(to,disabled,from);
        this.username = username;
        this.password = password;
        properties = new Properties();
        properties.setProperty("mail.smtp.host", "smtp.zoho.eu");
        properties.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        properties.setProperty("mail.smtp.socketFactory.fallback", "false");
        properties.setProperty("mail.smtp.port", "465");
        properties.setProperty("mail.smtp.socketFactory.port", "465");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.auth", "true");
        //properties.put("mail.debug", "true");
        properties.put("mail.store.protocol", "pop3");
        properties.put("mail.transport.protocol", "smtp");
        properties.put("mail.debug.auth", "true");
        properties.setProperty("mail.pop3.socketFactory.fallback", "false");
    }


    @Override
    public void sendHtmlEmail(String tos,String cc,String cci, String object, String htmlMessage, Map<File,String> fileMap) throws Exception{
        Session session = Session.getDefaultInstance(properties, new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });
        MimeMessage message = getMimeMessage(session,tos,cc,cci,object,htmlMessage,fileMap);
        Transport.send(message);
    }

}
