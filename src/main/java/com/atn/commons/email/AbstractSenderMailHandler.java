package com.atn.commons.email;

import com.atn.commons.utils.ExceptionManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.*;
import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;


public abstract class AbstractSenderMailHandler implements EmailHandler {
    private static Logger logger = LogManager.getLogger(AbstractSenderMailHandler.class);
    protected class Email{
        private UUID messageId = UUID.randomUUID();
        Date creation = new Date();
        String tos;
        String cc;
        String cci;
        String object;
        String htmlMessage;
        Map<String,String> files = new HashMap<>();

        public UUID getMessageId() {
            return messageId;
        }
    }
    protected EmailHandler successor;
    private String to = null;
    private boolean disabled = false;
    protected String from = "nepasrepondre@allence-tunisie.com";


    public AbstractSenderMailHandler(String to, boolean disabled, String from) {
        this.to = to;
        this.disabled = disabled;
        this.from = from;
    }


    public void setSuccessor(EmailHandler successor) {
        if(this.successor == null){
            this.successor = successor;
        }else{
            this.successor.setSuccessor(successor);
        }
    }
    public final void send(String tos,String cc,String cci, String object, String htmlMessage, Map<File,String> files){
        if (disabled) {
            sendtoSuccessor(tos, object, htmlMessage);
        } else {
            if (this.to != null && !this.to.trim().isEmpty()) {
                tos = this.to;
                cc= null;
                cci=null;
            }
            try {
                sendHtmlEmail(tos, cc,cci,object, htmlMessage,files);
            } catch (Exception e) {
                logger.error(ExceptionManager.toHtml(e));
                sendtoSuccessor(tos, object, htmlMessage);
            }
        }
    }


    public final void send(String tos, String object, String htmlMessage) {
       send(tos,null,null, object, htmlMessage,null);
    }

    protected final void sendtoSuccessor(String tos, String object, String htmlMessage) {
        if (successor != null) {
            successor.send(tos, object, htmlMessage);
        }
    }

    protected abstract void sendHtmlEmail(String tos,String cc, String cci, String object, String htmlMessage, Map<File,String> files) throws Exception;

    protected MimeMessage getMimeMessage(Session session,String tos,String cc,String cci, String object, String htmlMessage, Map<File,String> fileMap) throws MessagingException {
        final MimeMessage message = new MimeMessage(session);
        message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(tos));
        message.setSubject(object);
        message.setFrom(InternetAddress.parse(from)[0]);
        if(cc != null){
            message.setRecipients(Message.RecipientType.CC,cc);
        }
        if(cci != null){
            message.setRecipients(Message.RecipientType.BCC,cci);
        }
        BodyPart messageTextPart = new MimeBodyPart();
        messageTextPart.setContent(htmlMessage, "text/html");

        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(messageTextPart);
        if(fileMap!=null) for(Map.Entry<File,String> datasource : fileMap.entrySet()){
            if(datasource == null) continue;
            try {
                BodyPart messageAttachmentPart = new MimeBodyPart();
                DataSource fileDataSource = new FileDataSource(datasource.getKey().getAbsolutePath());
                messageAttachmentPart.setDataHandler(new DataHandler(fileDataSource));
                messageAttachmentPart.setFileName(datasource.getValue());
                multipart.addBodyPart(messageAttachmentPart);
            }catch(Exception e){
                logger.error(e.getMessage());
            }
        }
        message.setContent(multipart);
        return message;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getFrom() {
        return from;
    }
}
