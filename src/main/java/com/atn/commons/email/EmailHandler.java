package com.atn.commons.email;

import java.io.File;
import java.util.Map;

public interface EmailHandler {
    void send(String tos, String object, String htmlMessage);
    void send(String tos, String cc, String cci, String object, String htmlMessage, Map<File, String> files);
    void setSuccessor(EmailHandler successor);
}
