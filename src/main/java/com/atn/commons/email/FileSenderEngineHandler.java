package com.atn.commons.email;

import com.atn.commons.CommonsConfig;
import com.atn.commons.EmailConfig;
import com.atn.commons.utils.FileManager;
import com.atn.commons.utils.GsonManager;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import org.apache.commons.lang.ArrayUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import javax.activation.DataSource;
import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;


public class FileSenderEngineHandler extends AbstractSenderMailHandler {
    private static Logger logger = LogManager.getLogger(FileSenderEngineHandler.class);


    private String rootPath;

    /***
     *
     * @param rootPath
     *
     */
    public FileSenderEngineHandler(String rootPath, String to, boolean disabled, String from) {
        super(to, disabled, from);
        this.rootPath = rootPath;
        try {
            FileManager.forceMkdir(rootPath);
        } catch (IOException e) {
            logger.error(e.getMessage());notifyAll();

        }
    }

    @Override
    protected void sendHtmlEmail(String tos,String cc,String cci, String object, String htmlMessage, Map<File,String> fileMap) throws Exception {

        Email email = new Email();
        email.tos=tos;
        email.cc=cc;
        email.cci=cci;
        email.object=object;
        email.htmlMessage=htmlMessage;
        if(fileMap != null)for(Map.Entry<File,String> entry : fileMap.entrySet()){
            email.files.put(entry.getKey().getAbsolutePath(),entry.getValue());
        }
        String uglyJSONString = GsonManager.write(email);
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(uglyJSONString);
        String prettyJsonString = gson.toJson(je);
        logger.debug(prettyJsonString);
        FileManager.writeFile(rootPath+File.separator+email.getMessageId()+".json",prettyJsonString);
    }

    public static void main(String [] args) {
        if (args.length != 4) {
            logger.error("inFolderPath archiveFolderPath errorFolderPath configFileFolderPath" +
                    ", No good args : " + ArrayUtils.toMap(args));
            return;
        }
        ///   /DATA/COURTAGE/TODO/ /DATA/COURTAGE/DONE/ /DATA/COURTAGE/ERROR/ DATA/mail/conf/
        String inFolderPath = args[0];
        String archiveFolderPath = args[1];
        String errorFolderPath = args[2];
        String configFileFolderPath = args[3];//"/Users/walidmansia/Documents/STARTUPS/workspace/Commons/target/test-classes"
        String checkFilePath = inFolderPath + "/sendmail.lock";
        File checkFile = new File(checkFilePath);
        if (checkFile.exists()) {
            logger.error("Send Mail is Locked : sendmail.lock is present");
            return;
        } else {
            try {
                checkFile.createNewFile();
            } catch (IOException e) {
                logger.error("Error Creating lockFile : " + e.getMessage());
            }
        }
        File inFolder = new File(inFolderPath);
        if (!inFolder.isDirectory()) {
            logger.error("inFolder is not a Folder: " + inFolderPath);
            return;
        }
        File archiveFolder = new File(archiveFolderPath);
        if (!archiveFolder.isDirectory()) {
            logger.error("archiveFolderPath is not a Folder: " + archiveFolderPath);
            return;
        }
        File errorFolder = new File(errorFolderPath);
        if (!errorFolder.isDirectory()) {
            logger.error("errorFolderPath is not a Folder: " + errorFolderPath);
            return;
        }
        File configFolder = new File(configFileFolderPath);
        if (!configFolder.isDirectory()) {
            logger.error("errorFolderPath is not a Folder: " + configFileFolderPath);
            return;
        }
        try {
            addURL(configFileFolderPath);
        } catch (IOException e) {
            logger.error("unable to add to classPath configFolder " + e.getMessage());
            return;
        }
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(EmailConfig.class);
        EmailHandler emailHandler = applicationContext.getBean(EmailHandler.class);

        for (File mailFile : inFolder.listFiles()) {
            if(mailFile.getName().equals("sendmail.lock"))continue;
            try {
                String mailDtoString = FileManager.readFile(mailFile);
                Email email = GsonManager.read(Email.class, mailDtoString);
                Map<File,String> fileMap = new HashMap<>();
                for(Map.Entry<String,String> entry : email.files.entrySet()){
                    fileMap.put(new File(entry.getKey()),entry.getValue());
                }
                emailHandler.send(email.tos,email.cc,email.cci,email.object,email.htmlMessage,fileMap);
                FileManager.moveFileToFolder(archiveFolderPath, mailFile);
            } catch (Exception e) {
                try {
                    FileManager.moveFileToFolder(errorFolderPath, mailFile);
                } catch (IOException e1) {
                    logger.error("impossible to move " + mailFile.getAbsolutePath() + " -> " + errorFolderPath +
                            " : " + e1.getMessage());
                }
            }

        }
        FileManager.deleteFile(checkFilePath);

    }
    private static final Class[] parameters = new Class[]{URL.class};

    public static void addURL(String path) throws IOException {
        File f = new File(path);
        URL u = f.toURL();
        URLClassLoader sysloader = (URLClassLoader) ClassLoader.getSystemClassLoader();
        Class sysclass = URLClassLoader.class;

        try {
            Method method = sysclass.getDeclaredMethod("addURL", parameters);
            method.setAccessible(true);
            method.invoke(sysloader, new Object[]{u});
        } catch (Throwable t) {
            t.printStackTrace();
            throw new IOException("Error, could not add URL to system classloader");
        }//end try catch

    }
}