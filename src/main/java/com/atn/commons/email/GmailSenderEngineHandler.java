package com.atn.commons.email;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.File;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;


public class GmailSenderEngineHandler extends AbstractSenderMailHandler {
    private static Logger logger = LogManager.getLogger(GmailSenderEngineHandler.class);
    private Properties properties = new Properties();
    private String username;
    private String password;

    /***
     *
     * @param username
     * @param password
     *
     */
    public GmailSenderEngineHandler(String username, String password, String to, boolean disabled, String from) {
        super(to, disabled, from);

        this.username = username;
        this.password = password;
        properties.put("mail.smtp.host", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", "smtp.gmail.com");
        properties.put("mail.smtp.port", "587");
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.ssl.trust", "*");


    }

    @Override
    protected void sendHtmlEmail(String tos,String cc,String cci, String object, String htmlMessage, Map<File,String> fileMap) throws Exception {
        final Session session = Session.getInstance(properties, new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(GmailSenderEngineHandler.this.username, GmailSenderEngineHandler.this.password);
            }
        });
        if(logger.isDebugEnabled()){
            session.setDebug(true);
        }
        final MimeMessage message = getMimeMessage(session,tos,cc,cci,object,htmlMessage,fileMap);

        Transport.send(message);

    }


}