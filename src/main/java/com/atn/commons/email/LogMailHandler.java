package com.atn.commons.email;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.activation.DataSource;
import java.io.File;
import java.io.InputStream;
import java.util.Map;

public class LogMailHandler extends AbstractSenderMailHandler {
    private static Logger logger = LogManager.getLogger(LogMailHandler.class);

    public LogMailHandler() {
        super(null, false, "logger@logger.com");
    }

    @Override
    public void sendHtmlEmail(String tos,String cc,String cci, String object, String htmlMessage, Map<File,String> fileMap) {
        logger.info("*****************************************************");
        logger.info("*****************************************************");
        logger.info("objet : " + object);
        logger.info("*****************************************************");
        logger.info("tos : " + tos);
        logger.info("*****************************************************");
        logger.info("cc : " + cc);
        logger.info("*****************************************************");
        logger.info("cci : " + cci);
        logger.info("*****************************************************");
        logger.info("htmlMessage : " + htmlMessage);
        logger.info("*****************************************************");
        logger.info("*****************************************************");
        if(fileMap != null){
            logger.info("File : " + fileMap.size() + " files");
            logger.info("*****************************************************");
            logger.info("*****************************************************");
            for(Map.Entry<File,String> datasource : fileMap.entrySet()){
                logger.info(" Adding File ( "+datasource.getValue()+") to mail" );
                logger.info("*****************************************************");
                logger.info("*****************************************************");
            }
        }
    }
}
