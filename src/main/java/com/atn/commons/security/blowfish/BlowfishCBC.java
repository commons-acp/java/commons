package com.atn.commons.security.blowfish;
/**
 * @author <a href="mailto:lotfi.rhouma@allence-tunisie.com">RHOUMA Lotfi</a>
 * @version $Id$
 * @since 1.0 - 21 aout 2013 - 13:35:03
 */
public class BlowfishCBC extends BlowfishECB{

    int m_nIVLo;

    int m_nIVHi;

    public BlowfishCBC(byte[] key, int nOfs, int nLen, long lInitCBCIV) {
        super(key, nOfs, nLen);
        setCBCIV(lInitCBCIV);
    }

    public void setCBCIV(long lNewCBCIV) {
        m_nIVHi = BinConverter.longHi32(lNewCBCIV);
        m_nIVLo = BinConverter.longLo32(lNewCBCIV);
    }

    public int encrypt(byte[] inBuf, int nInPos, byte[] outBuf, int nOutPos,
                       int nLen) {
        // same speed tricks than in the ECB variant ...

        nLen -= nLen % BLOCKSIZE;

        int nC = nInPos + nLen;

        int[] pbox = m_pbox;
        int nPBox00 = pbox[0];
        int nPBox01 = pbox[1];
        int nPBox02 = pbox[2];
        int nPBox03 = pbox[3];
        int nPBox04 = pbox[4];
        int nPBox05 = pbox[5];
        int nPBox06 = pbox[6];
        int nPBox07 = pbox[7];
        int nPBox08 = pbox[8];
        int nPBox09 = pbox[9];
        int nPBox10 = pbox[10];
        int nPBox11 = pbox[11];
        int nPBox12 = pbox[12];
        int nPBox13 = pbox[13];
        int nPBox14 = pbox[14];
        int nPBox15 = pbox[15];
        int nPBox16 = pbox[16];
        int nPBox17 = pbox[17];

        int[] sbox1 = m_sbox1;
        int[] sbox2 = m_sbox2;
        int[] sbox3 = m_sbox3;
        int[] sbox4 = m_sbox4;

        int nIVHi = m_nIVHi;
        int nIVLo = m_nIVLo;

        int nHi, nLo;

        while (nInPos < nC) {
            nHi = inBuf[nInPos++] << 24;
            nHi |= (inBuf[nInPos++] << 16) & 0x0ff0000;
            nHi |= (inBuf[nInPos++] << 8) & 0x000ff00;
            nHi |= inBuf[nInPos++] & 0x00000ff;

            nLo = inBuf[nInPos++] << 24;
            nLo |= (inBuf[nInPos++] << 16) & 0x0ff0000;
            nLo |= (inBuf[nInPos++] << 8) & 0x000ff00;
            nLo |= inBuf[nInPos++] & 0x00000ff;

            // extra step: chain with IV

            nHi ^= nIVHi;
            nLo ^= nIVLo;

            nHi ^= nPBox00;
            nLo ^= (((sbox1[nHi >>> 24] + sbox2[(nHi >>> 16) & 0x0ff]) ^ sbox3[(nHi >>> 8) & 0x0ff]) + sbox4[nHi & 0x0ff])
                    ^ nPBox01;
            nHi ^= (((sbox1[nLo >>> 24] + sbox2[(nLo >>> 16) & 0x0ff]) ^ sbox3[(nLo >>> 8) & 0x0ff]) + sbox4[nLo & 0x0ff])
                    ^ nPBox02;
            nLo ^= (((sbox1[nHi >>> 24] + sbox2[(nHi >>> 16) & 0x0ff]) ^ sbox3[(nHi >>> 8) & 0x0ff]) + sbox4[nHi & 0x0ff])
                    ^ nPBox03;
            nHi ^= (((sbox1[nLo >>> 24] + sbox2[(nLo >>> 16) & 0x0ff]) ^ sbox3[(nLo >>> 8) & 0x0ff]) + sbox4[nLo & 0x0ff])
                    ^ nPBox04;
            nLo ^= (((sbox1[nHi >>> 24] + sbox2[(nHi >>> 16) & 0x0ff]) ^ sbox3[(nHi >>> 8) & 0x0ff]) + sbox4[nHi & 0x0ff])
                    ^ nPBox05;
            nHi ^= (((sbox1[nLo >>> 24] + sbox2[(nLo >>> 16) & 0x0ff]) ^ sbox3[(nLo >>> 8) & 0x0ff]) + sbox4[nLo & 0x0ff])
                    ^ nPBox06;
            nLo ^= (((sbox1[nHi >>> 24] + sbox2[(nHi >>> 16) & 0x0ff]) ^ sbox3[(nHi >>> 8) & 0x0ff]) + sbox4[nHi & 0x0ff])
                    ^ nPBox07;
            nHi ^= (((sbox1[nLo >>> 24] + sbox2[(nLo >>> 16) & 0x0ff]) ^ sbox3[(nLo >>> 8) & 0x0ff]) + sbox4[nLo & 0x0ff])
                    ^ nPBox08;
            nLo ^= (((sbox1[nHi >>> 24] + sbox2[(nHi >>> 16) & 0x0ff]) ^ sbox3[(nHi >>> 8) & 0x0ff]) + sbox4[nHi & 0x0ff])
                    ^ nPBox09;
            nHi ^= (((sbox1[nLo >>> 24] + sbox2[(nLo >>> 16) & 0x0ff]) ^ sbox3[(nLo >>> 8) & 0x0ff]) + sbox4[nLo & 0x0ff])
                    ^ nPBox10;
            nLo ^= (((sbox1[nHi >>> 24] + sbox2[(nHi >>> 16) & 0x0ff]) ^ sbox3[(nHi >>> 8) & 0x0ff]) + sbox4[nHi & 0x0ff])
                    ^ nPBox11;
            nHi ^= (((sbox1[nLo >>> 24] + sbox2[(nLo >>> 16) & 0x0ff]) ^ sbox3[(nLo >>> 8) & 0x0ff]) + sbox4[nLo & 0x0ff])
                    ^ nPBox12;
            nLo ^= (((sbox1[nHi >>> 24] + sbox2[(nHi >>> 16) & 0x0ff]) ^ sbox3[(nHi >>> 8) & 0x0ff]) + sbox4[nHi & 0x0ff])
                    ^ nPBox13;
            nHi ^= (((sbox1[nLo >>> 24] + sbox2[(nLo >>> 16) & 0x0ff]) ^ sbox3[(nLo >>> 8) & 0x0ff]) + sbox4[nLo & 0x0ff])
                    ^ nPBox14;
            nLo ^= (((sbox1[nHi >>> 24] + sbox2[(nHi >>> 16) & 0x0ff]) ^ sbox3[(nHi >>> 8) & 0x0ff]) + sbox4[nHi & 0x0ff])
                    ^ nPBox15;
            nHi ^= (((sbox1[nLo >>> 24] + sbox2[(nLo >>> 16) & 0x0ff]) ^ sbox3[(nLo >>> 8) & 0x0ff]) + sbox4[nLo & 0x0ff])
                    ^ nPBox16;

            nLo ^= nPBox17;

            outBuf[nOutPos++] = (byte) (nLo >>> 24);
            outBuf[nOutPos++] = (byte) (nLo >>> 16);
            outBuf[nOutPos++] = (byte) (nLo >>> 8);
            outBuf[nOutPos++] = (byte) nLo;

            outBuf[nOutPos++] = (byte) (nHi >>> 24);
            outBuf[nOutPos++] = (byte) (nHi >>> 16);
            outBuf[nOutPos++] = (byte) (nHi >>> 8);
            outBuf[nOutPos++] = (byte) nHi;

            // (the encrypted block becomes the new IV)

            nIVHi = nLo;
            nIVLo = nHi;
        }

        m_nIVHi = nIVHi;
        m_nIVLo = nIVLo;

        return nLen;
    }

    public void setCBCIV(byte[] newCBCIV, int nOfs) {
        m_nIVHi = BinConverter.byteArrayToInt(newCBCIV, nOfs);
        m_nIVLo = BinConverter.byteArrayToInt(newCBCIV, nOfs + 4);
    }

    public int decrypt(byte[] inBuf, int nInPos, byte[] outBuf, int nOutPos,
                       int nLen) {
        nLen -= nLen % BLOCKSIZE;

        int nC = nInPos + nLen;

        int[] pbox = m_pbox;
        int nPBox00 = pbox[0];
        int nPBox01 = pbox[1];
        int nPBox02 = pbox[2];
        int nPBox03 = pbox[3];
        int nPBox04 = pbox[4];
        int nPBox05 = pbox[5];
        int nPBox06 = pbox[6];
        int nPBox07 = pbox[7];
        int nPBox08 = pbox[8];
        int nPBox09 = pbox[9];
        int nPBox10 = pbox[10];
        int nPBox11 = pbox[11];
        int nPBox12 = pbox[12];
        int nPBox13 = pbox[13];
        int nPBox14 = pbox[14];
        int nPBox15 = pbox[15];
        int nPBox16 = pbox[16];
        int nPBox17 = pbox[17];

        int[] sbox1 = m_sbox1;
        int[] sbox2 = m_sbox2;
        int[] sbox3 = m_sbox3;
        int[] sbox4 = m_sbox4;

        int nIVHi = m_nIVHi;
        int nIVLo = m_nIVLo;

        int nTmpHi, nTmpLo;

        int nHi, nLo;

        while (nInPos < nC) {
            nHi = inBuf[nInPos++] << 24;
            nHi |= (inBuf[nInPos++] << 16) & 0x0ff0000;
            nHi |= (inBuf[nInPos++] << 8) & 0x000ff00;
            nHi |= inBuf[nInPos++] & 0x00000ff;

            nLo = inBuf[nInPos++] << 24;
            nLo |= (inBuf[nInPos++] << 16) & 0x0ff0000;
            nLo |= (inBuf[nInPos++] << 8) & 0x000ff00;
            nLo |= inBuf[nInPos++] & 0x00000ff;

            // (save the current block, it will become the new IV)
            nTmpHi = nHi;
            nTmpLo = nLo;

            nHi ^= nPBox17;
            nLo ^= (((sbox1[nHi >>> 24] + sbox2[(nHi >>> 16) & 0x0ff]) ^ sbox3[(nHi >>> 8) & 0x0ff]) + sbox4[nHi & 0x0ff])
                    ^ nPBox16;
            nHi ^= (((sbox1[nLo >>> 24] + sbox2[(nLo >>> 16) & 0x0ff]) ^ sbox3[(nLo >>> 8) & 0x0ff]) + sbox4[nLo & 0x0ff])
                    ^ nPBox15;
            nLo ^= (((sbox1[nHi >>> 24] + sbox2[(nHi >>> 16) & 0x0ff]) ^ sbox3[(nHi >>> 8) & 0x0ff]) + sbox4[nHi & 0x0ff])
                    ^ nPBox14;
            nHi ^= (((sbox1[nLo >>> 24] + sbox2[(nLo >>> 16) & 0x0ff]) ^ sbox3[(nLo >>> 8) & 0x0ff]) + sbox4[nLo & 0x0ff])
                    ^ nPBox13;
            nLo ^= (((sbox1[nHi >>> 24] + sbox2[(nHi >>> 16) & 0x0ff]) ^ sbox3[(nHi >>> 8) & 0x0ff]) + sbox4[nHi & 0x0ff])
                    ^ nPBox12;
            nHi ^= (((sbox1[nLo >>> 24] + sbox2[(nLo >>> 16) & 0x0ff]) ^ sbox3[(nLo >>> 8) & 0x0ff]) + sbox4[nLo & 0x0ff])
                    ^ nPBox11;
            nLo ^= (((sbox1[nHi >>> 24] + sbox2[(nHi >>> 16) & 0x0ff]) ^ sbox3[(nHi >>> 8) & 0x0ff]) + sbox4[nHi & 0x0ff])
                    ^ nPBox10;
            nHi ^= (((sbox1[nLo >>> 24] + sbox2[(nLo >>> 16) & 0x0ff]) ^ sbox3[(nLo >>> 8) & 0x0ff]) + sbox4[nLo & 0x0ff])
                    ^ nPBox09;
            nLo ^= (((sbox1[nHi >>> 24] + sbox2[(nHi >>> 16) & 0x0ff]) ^ sbox3[(nHi >>> 8) & 0x0ff]) + sbox4[nHi & 0x0ff])
                    ^ nPBox08;
            nHi ^= (((sbox1[nLo >>> 24] + sbox2[(nLo >>> 16) & 0x0ff]) ^ sbox3[(nLo >>> 8) & 0x0ff]) + sbox4[nLo & 0x0ff])
                    ^ nPBox07;
            nLo ^= (((sbox1[nHi >>> 24] + sbox2[(nHi >>> 16) & 0x0ff]) ^ sbox3[(nHi >>> 8) & 0x0ff]) + sbox4[nHi & 0x0ff])
                    ^ nPBox06;
            nHi ^= (((sbox1[nLo >>> 24] + sbox2[(nLo >>> 16) & 0x0ff]) ^ sbox3[(nLo >>> 8) & 0x0ff]) + sbox4[nLo & 0x0ff])
                    ^ nPBox05;
            nLo ^= (((sbox1[nHi >>> 24] + sbox2[(nHi >>> 16) & 0x0ff]) ^ sbox3[(nHi >>> 8) & 0x0ff]) + sbox4[nHi & 0x0ff])
                    ^ nPBox04;
            nHi ^= (((sbox1[nLo >>> 24] + sbox2[(nLo >>> 16) & 0x0ff]) ^ sbox3[(nLo >>> 8) & 0x0ff]) + sbox4[nLo & 0x0ff])
                    ^ nPBox03;
            nLo ^= (((sbox1[nHi >>> 24] + sbox2[(nHi >>> 16) & 0x0ff]) ^ sbox3[(nHi >>> 8) & 0x0ff]) + sbox4[nHi & 0x0ff])
                    ^ nPBox02;
            nHi ^= (((sbox1[nLo >>> 24] + sbox2[(nLo >>> 16) & 0x0ff]) ^ sbox3[(nLo >>> 8) & 0x0ff]) + sbox4[nLo & 0x0ff])
                    ^ nPBox01;

            nLo ^= nPBox00;

            // extra step: unchain

            nHi ^= nIVLo;
            nLo ^= nIVHi;

            outBuf[nOutPos++] = (byte) (nLo >>> 24);
            outBuf[nOutPos++] = (byte) (nLo >>> 16);
            outBuf[nOutPos++] = (byte) (nLo >>> 8);
            outBuf[nOutPos++] = (byte) nLo;

            outBuf[nOutPos++] = (byte) (nHi >>> 24);
            outBuf[nOutPos++] = (byte) (nHi >>> 16);
            outBuf[nOutPos++] = (byte) (nHi >>> 8);
            outBuf[nOutPos++] = (byte) nHi;

            // (now set the new IV)
            nIVHi = nTmpHi;
            nIVLo = nTmpLo;
        }

        m_nIVHi = nIVHi;
        m_nIVLo = nIVLo;

        return nLen;
    }

}
