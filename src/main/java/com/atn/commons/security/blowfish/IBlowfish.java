package com.atn.commons.security.blowfish;

import java.util.Date;


/**
 * @author <a href="mailto:lotfi.rhouma@allence-tunisie.com">RHOUMA Lotfi</a>
 * @version $Id$
 * @since 1.0 - 21 aout 2013 - 13:30:23
 */
public interface IBlowfish {

    // encrypt password method
    String encrypt(Date date, String sPlainText);

    // decrypt password method
    String decrypt(Date date, String sCipherText);
}
