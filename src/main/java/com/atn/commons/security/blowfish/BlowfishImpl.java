package com.atn.commons.security.blowfish;

import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * @author <a href="mailto:lotfi.rhouma@allence-tunisie.com">RHOUMA Lotfi</a>
 * @version $Id$
 * @since 1.0 - 21 aout 2013 - 13:32:19
 */
public class BlowfishImpl implements IBlowfish {

    private BlowfishCBC m_bfc;

    static final SecureRandom _srnd;

    static {
        _srnd = new SecureRandom();
    }

    private String standardConvert( final Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        return sdf.format(date);
    }

    // encrypt password method
    /* (non-Javadoc)
      * @see com.allence.commons.security.blowfish.BlowfishI#encrypt(java.lang.String, java.lang.String)
      */
    public String encrypt( final Date date,  final String sPlainText) {
        long lCBCIV;

        synchronized (_srnd) {
            lCBCIV = _srnd.nextLong();
        }

        String key = this.standardConvert(date);

        this.keyStringToBit(key.toCharArray());
        return this.encStr(sPlainText, lCBCIV);
    }

    // decrypt password method
    /* (non-Javadoc)
      * @see com.allence.commons.security.blowfish.BlowfishI#decrypt(java.lang.String)
      */
    public String decrypt( Date date,  String sCipherText) {
        int nNumOfBytes, nPadByte, nLen;
        byte[] buf;
        byte[] cbciv;

        String key = this.standardConvert(date);

        this.keyStringToBit(key.toCharArray());

        nLen = (sCipherText.length() >> 1) & ~7;

        if (BlowfishECB.BLOCKSIZE > nLen) {
            return null;
        }

        cbciv = new byte[BlowfishCBC.BLOCKSIZE];

        nNumOfBytes = BinConverter.hexStrToBytes(sCipherText, cbciv, 0, 0,
                BlowfishCBC.BLOCKSIZE);

        if (nNumOfBytes < BlowfishCBC.BLOCKSIZE)
            return null;

        m_bfc.setCBCIV(cbciv, 0);

        nLen -= BlowfishCBC.BLOCKSIZE;
        if (nLen == 0) {
            return "";
        }

        buf = new byte[nLen];

        nNumOfBytes = BinConverter.hexStrToBytes(sCipherText, buf,
                BlowfishCBC.BLOCKSIZE << 1, 0, nLen);

        if (nNumOfBytes < nLen) {
            return null;
        }

        m_bfc.decrypt(buf, 0, buf, 0, buf.length);

        nPadByte = buf[buf.length - 1] & 0x0ff;

        // (try to get everything, even if the padding seem to be wrong)
        if (BlowfishCBC.BLOCKSIZE < nPadByte) {
            nPadByte = 0;
        }

        nNumOfBytes -= nPadByte;

        if (nNumOfBytes < 0) {
            return "";
        }

        return BinConverter.byteArrayToStr(buf, 0, nNumOfBytes);
    }

    private void keyStringToBit( final char[] passw) {
        int nI, nC;
        SHA1 sh = null;
        byte[] hash;
        // hash down the password to a 160bit key, using SHA-1
        sh = new SHA1();
        for (nI = 0, nC = passw.length; nI < nC; nI++) {
            sh.update((byte) ((passw[nI] >> 8) & 0x0ff));
            sh.update((byte) (passw[nI] & 0x0ff));
        }
        sh.finalize();
        // setup the encryptor (using a dummy IV for now)
        hash = new byte[SHA1.DIGEST_SIZE];
        sh.getDigest(hash, 0);

        m_bfc = new BlowfishCBC(hash, 0, hash.length, 0);
    }

    private String encStr( final String sPlainText, final long lNewCBCIV) {
        int nI, nPos, nStrLen;
        char cActChar;
        byte bPadVal;
        byte[] buf;
        byte[] newCBCIV;

        nStrLen = sPlainText.length();
        buf = new byte[((nStrLen << 1) & ~7) + 8];

        nPos = 0;
        for (nI = 0; nI < nStrLen; nI++) {
            cActChar = sPlainText.charAt(nI);
            buf[nPos++] = (byte) ((cActChar >> 8) & 0x0ff);
            buf[nPos++] = (byte) (cActChar & 0x0ff);
        }

        bPadVal = (byte) (buf.length - (nStrLen << 1));
        while (nPos < buf.length) {
            buf[nPos++] = bPadVal;
        }

        m_bfc.setCBCIV(lNewCBCIV);

        m_bfc.encrypt(buf, 0, buf, 0, buf.length);

        newCBCIV = new byte[BlowfishCBC.BLOCKSIZE];

        BinConverter.longToByteArray(lNewCBCIV, newCBCIV, 0);

        return BinConverter.bytesToHexStr(newCBCIV, 0, BlowfishCBC.BLOCKSIZE)
                + BinConverter.bytesToHexStr(buf, 0, buf.length);
    }
}
