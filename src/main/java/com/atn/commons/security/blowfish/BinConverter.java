package com.atn.commons.security.blowfish;
/**
 * @author <a href="mailto:lotfi.rhouma@allence-tunisie.com">RHOUMA Lotfi</a>
 * @version $Id$
 * @since 1.0 - 21 aout 2013 - 13:36:50
 */
public class BinConverter {
    /**
     * Gets bytes from an array into an integer.
     *
     * @param buf  where to get the bytes
     * @param nOfs index from where to read the data
     * @return the 32bit integer
     */
    public final static int byteArrayToInt(byte[] buf, int nOfs) {
        return (buf[nOfs] << 24) | ((buf[nOfs + 1] & 0x0ff) << 16)
                | ((buf[nOfs + 2] & 0x0ff) << 8) | (buf[nOfs + 3] & 0x0ff);
    }

    public final static int longHi32(long lVal) {
        return (int) (lVal >>> 32);
    }

    public final static int longLo32(long lVal) {
        return (int) lVal;
    }

    public final static void longToByteArray(long lValue, byte[] buf, int nOfs) {
        int nTmp = (int) (lValue >>> 32);

        buf[nOfs] = (byte) (nTmp >>> 24);
        buf[nOfs + 1] = (byte) ((nTmp >>> 16) & 0x0ff);
        buf[nOfs + 2] = (byte) ((nTmp >>> 8) & 0x0ff);
        buf[nOfs + 3] = (byte) nTmp;

        nTmp = (int) lValue;

        buf[nOfs + 4] = (byte) (nTmp >>> 24);
        buf[nOfs + 5] = (byte) ((nTmp >>> 16) & 0x0ff);
        buf[nOfs + 6] = (byte) ((nTmp >>> 8) & 0x0ff);
        buf[nOfs + 7] = (byte) nTmp;
    }

    public final static String bytesToHexStr(byte[] data, int nOfs, int nLen) {
        StringBuffer sbuf;

        sbuf = new StringBuffer();
        sbuf.setLength(nLen << 1);

        int nPos = 0;

        int nC = nOfs + nLen;

        while (nOfs < nC) {
            sbuf.setCharAt(nPos++, HEXTAB[(data[nOfs] >> 4) & 0x0f]);
            sbuf.setCharAt(nPos++, HEXTAB[data[nOfs++] & 0x0f]);
        }
        return sbuf.toString();
    }

    final static char[] HEXTAB = {'0', '1', '2', '3', '4', '5', '6', '7', '8',
            '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    public final static int hexStrToBytes(String sHex, byte[] data,
                                          int nSrcOfs, int nDstOfs, int nLen) {
        int nI, nJ, nStrLen, nAvailBytes, nDstOfsBak;
        byte bActByte;
        boolean blConvertOK;

        // check for correct ranges
        nStrLen = sHex.length();

        nAvailBytes = (nStrLen - nSrcOfs) >> 1;
        if (nAvailBytes < nLen) {
            nLen = nAvailBytes;
        }

        int nOutputCapacity = data.length - nDstOfs;
        if (nLen > nOutputCapacity) {
            nLen = nOutputCapacity;
        }

        // convert now

        nDstOfsBak = nDstOfs;

        for (nI = 0; nI < nLen; nI++) {
            bActByte = 0;
            blConvertOK = true;

            for (nJ = 0; nJ < 2; nJ++) {
                bActByte <<= 4;
                char cActChar = sHex.charAt(nSrcOfs++);

                if (cActChar >= 'a' && cActChar <= 'f') {
                    bActByte |= (byte) (cActChar - 'a') + 10;
                } else {
                    if (cActChar >= '0' && cActChar <= '9') {
                        bActByte |= (byte) (cActChar - '0');
                    } else {
                        blConvertOK = false;
                    }
                }
            }
            if (blConvertOK) {
                data[nDstOfs++] = bActByte;
            }
        }

        return nDstOfs - nDstOfsBak;
    }

    public final static String byteArrayToStr(byte[] data, int nOfs, int nLen) {
        int nAvailCapacity, nSBufPos;
        StringBuffer sbuf;

        // we need two bytes for every character
        nLen &= ~1;

        // enough bytes in the buf?
        nAvailCapacity = data.length - nOfs;

        if (nAvailCapacity < nLen) {
            nLen = nAvailCapacity;
        }

        sbuf = new StringBuffer();
        sbuf.setLength(nLen >> 1);
//		sbuf.setLength(21 >> 1);

        nSBufPos = 0;

        while (0 < nLen) {
            sbuf.setCharAt(nSBufPos++,
                    (char) ((data[nOfs] << 8) | (data[nOfs + 1] & 0x0ff)));
            nOfs += 2;
            nLen -= 2;
        }

        return sbuf.toString();
    }

}
