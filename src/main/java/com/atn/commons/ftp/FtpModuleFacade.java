package com.atn.commons.ftp;

import java.io.File;
import java.io.IOException;

/**
 * @author <a href="mailto:lotfi.rhouma@allence-tunisie.com">Lotfi RHOUMA</a>
 * @version $Id: FtpModuleFacade.java 277 2013-01-29 10:15:35Z lotfi $
 * @since 1.0 - 1 oct. 2012 - 08:49:47
 */
public interface FtpModuleFacade {
	
/**
 * 
 * @throws IOException
 */
    void receive() throws  IOException;

    void send(File file) throws  IOException;
    

}
