package com.atn.commons.ftp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.SocketException;
import java.util.Arrays;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;

import com.atn.commons.utils.FileManager;

/**
 * @author <a href="mailto:lotfi.rhouma@allence-tunisie.com">Lotfi RHOUMA</a>
 * @version $Id: Ftp4cheFtpEngine.java 10744 2016-02-29 08:07:04Z wassim $
 * @since 1.0 - 1 oct. 2012 - 09:13:14
 */
public class FtpEngine implements FtpModuleFacade {

	private FTPClient ftpClient;

	private FtpModuleProperties ftpProperties = new FtpModuleProperties();

	@Override
	public void receive() throws IOException {
		connect();
		login();
		importData();
		logout();
		disconnect();
	}

	@Override
	public void send(File file) throws IOException {
		connect();
		login();
		exportData(file);
		logout();
		disconnect();
	}

	/**
	 * 
	 * @throws IOException
	 */
	private void importData() throws IOException {

		String remotePath = ftpProperties.getRemotePath();
		String localPath = ftpProperties.getLocalPath();
		// create local path if does not exists
		FileManager.forceMkdir(localPath);
		FTPFile[] fileList = ftpClient.listFiles(remotePath);
		Arrays.asList(fileList);
		for (FTPFile file : fileList) {
			if (!file.isFile()) {
				continue;
			}
			// get output stream
			OutputStream output;
			output = new FileOutputStream(localPath + "/" + file.getName());
			// get the file from the remote system
			ftpClient.retrieveFile(file.getName(), output);
			// close output stream
			output.close();
			// delete the file
			ftpClient.deleteFile(file.getName());
		}

	}

	/**
	 * 
	 * @param fileToFTP
	 * @throws IOException
	 */
	private void exportData(File fileToFTP) throws IOException {
		String remotePath = ftpProperties.getRemotePath();
		// change current directory
		ftpClient.changeWorkingDirectory(remotePath);
		// get input stream
		InputStream input;
		input = new FileInputStream(fileToFTP.getAbsolutePath());
		// store the file in the remote server
		ftpClient.storeFile(fileToFTP.getName(), input);
		// close the stream
		input.close();
	}

	/***
	 * 
	 * @throws IOException
	 */
	private void connect() throws IOException {
		ftpClient = new FTPClient();

		try {
			ftpClient.connect(ftpProperties.getHostName());
		} catch (SocketException socketException) {
			throw socketException;
		} catch (IOException ioException) {
			throw ioException;

		}

	}

	/***
	 * 
	 * @return
	 * @throws IOException
	 */
	private Boolean login() throws IOException {
		if (ftpClient != null && ftpClient.isConnected()) {
			return ftpClient.login(ftpProperties.getUsername(),
					ftpProperties.getPassword());
		}
		return Boolean.FALSE;
	}

	/***
	 * 
	 * @return
	 * @throws IOException
	 */
	private Boolean logout() throws IOException {
		if (ftpClient != null && ftpClient.isConnected()) {
			return ftpClient.logout();
		}
		return Boolean.FALSE;
	}

	/***
	 * 
	 * @throws IOException
	 */
	private void disconnect() throws IOException {
		if (ftpClient != null && ftpClient.isConnected())
			try {
				ftpClient.disconnect();
			} catch (IOException ioException) {
				throw ioException;
			}
	}
}
