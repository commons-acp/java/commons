package com.atn.commons.ftp;



/**
 * @author <a href="mailto:lotfi.rhouma@allence-tunisie.com">Lotfi RHOUMA</a>
 * @version $Id: FtpModuleUtils.java 10755 2016-03-02 10:54:06Z walid $
 * @since 1.0 - 1 oct. 2012 - 09:16:09
 */
public class FtpModuleUtils {

    public static String getServerSeparator(String path) {
        String serverSeparator;
        if (path.contains("/")) {
            serverSeparator = "/";
        } else {
            serverSeparator = "\\";
        }
        return serverSeparator;
    }

}
