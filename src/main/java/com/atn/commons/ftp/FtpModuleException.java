package com.atn.commons.ftp;
/**
 * @author <a href="mailto:lotfi.rhouma@allence-tunisie.com">Lotfi RHOUMA</a>
 * @version $Id: FtpModuleException.java 277 2013-01-29 10:15:35Z lotfi $
 * @since 1.0 - 1 oct. 2012 - 09:14:51
 */
public class FtpModuleException extends Exception {

    /**
	 * 
	 */
	private static final long serialVersionUID = -753433734362772839L;

	public FtpModuleException() {
        super();
    }

    public FtpModuleException(String message) {
        super(message);
    }

    public FtpModuleException(String message, Throwable cause) {
        super(message, cause);
    }

    public FtpModuleException(Throwable cause) {
        super(cause);
    }
}
