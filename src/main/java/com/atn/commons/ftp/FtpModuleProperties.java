package com.atn.commons.ftp;

import java.util.Properties;

/**
 * @author <a href="mailto:lotfi.rhouma@allence-tunisie.com">Lotfi RHOUMA</a>
 * @version $Id: FtpModuleProperties.java 277 2013-01-29 10:15:35Z lotfi $
 * @since 1.0 - 1 oct. 2012 - 10:07:48
 */
public class FtpModuleProperties extends Properties {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4343737416051111976L;

	public static final String SECURED_TYPE = "AUTH_SSL_FTP_CONNECTION";
	public static final String STD_TYPE = "FTP_CONNECTION";

	public String getHostName() {
		return super.getProperty("ftp.hostname");
	}

	public String getUsername() {
		return super.getProperty("ftp.username");
	}

	public String getPassword() {
		return super.getProperty("ftp.password");
	}

	public String getLocalPath() {
		if (super.getProperty("local.path") != null) {
			return super.getProperty("local.path");
		} else {
			return "C:/ntSupport/";
		}
	}

	public String getRemotePath() {
		if (super.getProperty("remote.path") != null) {
			return super.getProperty("remote.path");
		} else {
			return "";
		}
	}

	public String getConnectionType() {
		if (Boolean.parseBoolean(super.getProperty("connection.secure"))) {
			return SECURED_TYPE;
		} else {
			return STD_TYPE;
		}
	}

}
