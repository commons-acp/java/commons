package com.atn.commons;

import com.atn.commons.persistence.DBInitializer;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.context.annotation.*;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.orm.hibernate5.HibernateExceptionTranslator;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.support.PersistenceAnnotationBeanPostProcessor;
import org.springframework.orm.jpa.vendor.HibernateJpaDialect;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.annotation.PreDestroy;
import javax.sql.DataSource;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

@Configuration
@EnableAsync
@EnableScheduling
@EnableAspectJAutoProxy
@EnableTransactionManagement
@EnableCaching
@ComponentScan({"com.atn.*"})
@Import({EmailConfig.class , SmsConfig.class})
@PropertySource(value = "classpath:config.properties", ignoreResourceNotFound = false)
@PropertySource(value = "file:${contextConfigurationPath}", ignoreResourceNotFound = true)
public abstract class CommonsConfig {

    private static Logger logger = LogManager.getLogger(CommonsConfig.class);
    private static Properties configuration = null;
    private static String path = null;
    private String jdbcDriverClassName = configuration().getProperty("jdbc.driverClassName");
    private String jdbcHost = configuration().getProperty("jdbc.host");
    private String jdbcType = configuration().getProperty("jdbc.type");
    private String jdbcDbname = configuration().getProperty("jdbc.dbname");
    private String jdbcExtention = configuration().getProperty("jdbc.extention");
    private String jdbcUsername = configuration().getProperty("jdbc.username");
    private String jdbcPassword = configuration().getProperty("jdbc.password");
    private String hibernateDialect = configuration().getProperty("hibernate.dialect");
    private String hibernateShowSql = configuration().getProperty("hibernate.show_sql");


    protected static Properties configuration() {
        if (configuration == null) {
            configuration = new Properties();

            if (CommonsConfig.GetSystemConfigPath() != null) {

                try (InputStream input = new FileInputStream(path)) {
                    configuration.load(input);

                } catch (Exception ex) {
                    logger.error(ex);
                }
            } else {

                ResourcePatternResolver resourcePatternResolver = new PathMatchingResourcePatternResolver();
                try {
                    Resource[] resources = resourcePatternResolver.getResources("/*.properties");
                    for (int i = 0; i < resources.length; i++) {
                        configuration.load(resources[i].getInputStream());
                    }
                } catch (IOException ioe) {
                    logger.error(ioe);
                }
            }

        }

        return configuration;
    }

    protected static String GetSystemConfigPath() {
        if (path == null) {
            path = System.getenv("contextConfigurationPath");
        }

        return path;
    }

    @Bean()
    public DataSource dataSource() throws SQLException {
        String url = "jdbc:" + jdbcType + jdbcHost;
        DBInitializer.createDataBaseIfInexistant(jdbcDriverClassName, url, jdbcUsername, jdbcPassword, jdbcDbname);
        BasicDataSource ds = new BasicDataSource();
        ds.setDriverClassName(jdbcDriverClassName);
        url = "jdbc:" + jdbcType + jdbcHost + "" + jdbcDbname + jdbcExtention;
        ds.setUrl(url);
        ds.setUsername(jdbcUsername);
        ds.setPassword(jdbcPassword);
        ds.setValidationQuery("SELECT 1");
        ds.setTestOnBorrow(true);
        return ds;
    }

    @Bean
    public PersistenceExceptionTranslationPostProcessor exceptionTranslationPostProcessor() {
        return new PersistenceExceptionTranslationPostProcessor();
    }

    @Bean
    public HibernateExceptionTranslator hibernateExceptionTranslator() {
        return new HibernateExceptionTranslator();
    }

    @Bean(name = "transactionManager")
    public JpaTransactionManager transactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        return transactionManager;
    }

    @Bean
    public PersistenceAnnotationBeanPostProcessor persistenceAnnotationBeanPostProcessor() {
        PersistenceAnnotationBeanPostProcessor persistenceAnnotationBeanPostProcessor = new PersistenceAnnotationBeanPostProcessor();
        return persistenceAnnotationBeanPostProcessor;
    }

    @Bean(name = "entityManagerFactory")
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() throws SQLException {
        LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
        entityManagerFactoryBean
                .setPersistenceXmlLocation("classpath:META-INF/persistence.xml");
        entityManagerFactoryBean.setPersistenceUnitName("pu");
        entityManagerFactoryBean.setDataSource(dataSource());
        entityManagerFactoryBean.setJpaVendorAdapter(jpaVendorAdapter());
        entityManagerFactoryBean.setJpaDialect(jpaDialect());
        entityManagerFactoryBean.setJpaProperties(jpaProperties());
        return entityManagerFactoryBean;
    }

    @Bean(name = "jpaVendorAdapter")
    public HibernateJpaVendorAdapter jpaVendorAdapter() {
        HibernateJpaVendorAdapter jpaVendorAdapter = new HibernateJpaVendorAdapter();
        jpaVendorAdapter.setDatabasePlatform(hibernateDialect);
        return jpaVendorAdapter;
    }

    @Bean(name = "jpaDialect")
    public HibernateJpaDialect jpaDialect() {
        HibernateJpaDialect jpaDialect = new HibernateJpaDialect();
        return jpaDialect;
    }

    @Bean
    public Properties jpaProperties() {
        Properties properties = new Properties();
        properties.put("hibernate.dialect", hibernateDialect);
        properties.put("hibernate.show_sql", hibernateShowSql);
        return properties;
    }

    @Bean
    public CacheManager cacheManager() {
        logger.info("Starting Ehcache");
        cacheManager = net.sf.ehcache.CacheManager.create(CommonsConfig.class.getClassLoader()
                .getResourceAsStream("ehcache.xml"));
        EhCacheCacheManager ehCacheManager = new EhCacheCacheManager();
        ehCacheManager.setCacheManager(cacheManager);
        return ehCacheManager;
    }

    private net.sf.ehcache.CacheManager cacheManager;

    @PreDestroy
    public void destroy() {
        if (cacheManager != null) cacheManager.shutdown();
    }

    @Bean
    public DBInitializer dbInitializer(DataSource dataSource) {
        DBInitializer dbInitializer = null;
        try {
            dbInitializer = DBInitializer.getInstance(dataSource);
            dbInitializer.update();
        } catch (IOException e) {
            logger.error("IOException Updating base", e);
        } catch (SQLException e) {
            logger.error("SQLException Updating base", e);
        } catch (Exception e) {
            logger.error("Exception Updating base", e);
        }
        return dbInitializer;
    }

}
