package com.atn.commons.report.birt;

import java.io.OutputStream;
import java.util.Map;

import org.springframework.stereotype.Component;


@Component
public class ReportEngineImpl implements com.atn.commons.report.ReportEngine{

	@Override
	public void startup() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void shutdown() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void generate(String reportPath, OutputStream outputStream, REPORT_TYPE type) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void generate(String reportPath, OutputStream outputStream, REPORT_TYPE type, Map<String, Object> parameters)
			throws Exception {
		// TODO Auto-generated method stub
		
	}
	
//	private EngineConfig config ;
//	private ReportEngine engine ;
//	public ReportEngineImpl() {
//		config = new EngineConfig();
//	}
//
//	public void startup() throws Exception {
//		Platform.startup(config);
//		engine = new ReportEngine(config);
//	}
//
//	public void shutdown() {
//		engine.destroy();
//		Platform.shutdown();
//	}
//	public void generate(String reportPath,OutputStream outputStream, REPORT_TYPE type) throws Exception{
//		this.generate(reportPath, outputStream, type, new HashMap<String,Object>());
//	}
//	public void generate(String reportPath,OutputStream outputStream, REPORT_TYPE type,Map<String,Object> parameters) throws Exception{
//		IReportRunnable reportRunnable = engine
//				.openReportDesign(getClass().getResourceAsStream(reportPath));
//		IRunAndRenderTask runAndRender = engine.createRunAndRenderTask(reportRunnable);
//		
//		for(String key : parameters.keySet()){
//			runAndRender.setParameterValue(key, parameters.get(key));
//		}
//		PDFRenderOption option = new PDFRenderOption();
//		option.setOutputStream(outputStream);
//		option.setOutputFormat(type.toString());
//		runAndRender.setRenderOption(option);
//
//		runAndRender.run();
//		runAndRender.close();
//	}
}
