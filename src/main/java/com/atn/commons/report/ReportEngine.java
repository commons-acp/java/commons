package com.atn.commons.report;

import java.io.OutputStream;
import java.util.Map;


public interface ReportEngine {
	public enum REPORT_TYPE{
		DOC,PDF
	}
	public void startup() throws Exception;
	public void shutdown();
	public void generate(String reportPath, OutputStream outputStream, REPORT_TYPE type) throws Exception;
	public void generate(String reportPath, OutputStream outputStream, REPORT_TYPE type, Map<String, Object> parameters) throws Exception;
	
}
