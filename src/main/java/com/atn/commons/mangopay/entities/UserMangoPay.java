package com.atn.commons.mangopay.entities;


import java.io.Serializable;

/**
 * Provides core user information.
 *
 * <p>
 * Implementations are not used directly by Commons for security purposes. They
 * simply store user information which is later encapsulated into
 * objects. This allows non-mangoPay related user information (such as email addresses,
 * telephone numbers etc) to be stored in a convenient location.
 * <p>
 * Concrete implementations must take particular care to ensure the non-null contract
 * detailed for each method is enforced. See
 * {@link com.mangopay.entities.User} for a reference
 * implementation (which you might like to extend or use in your code).
 *
 * @see com.atn.commons.mangopay.service.CommonsMangoPayService
 * @see com.mangopay.entities.User
 *
 * @since commons version 3.76
 * @author Monta
 */

public interface UserMangoPay extends Serializable {


    /**
     * <p>The Mango pay id is the link between your user class and the mango pay account
     * it better if you keep the mango pay user id in your database  the others are to specify
     * you can also persist them but it will be the duplication from mango database</p>
     * Gets the mangoPay user's Id
     *
     * @return a <code> String </code>
     * specifying the users's id
     * @see com.mangopay.entities
     */

    String getMangoPayId();


}
