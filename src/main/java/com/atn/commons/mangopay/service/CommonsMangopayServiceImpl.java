package com.atn.commons.mangopay.service;

import com.atn.commons.mangopay.entities.UserMangoPay;
import com.mangopay.MangoPayApi;
import com.mangopay.core.Configuration;
import com.mangopay.core.Money;
import com.mangopay.core.enumerations.*;
import com.mangopay.entities.*;
import com.mangopay.entities.subentities.PayInExecutionDetailsWeb;
import com.mangopay.entities.subentities.PayInPaymentDetailsCard;
import com.mangopay.entities.subentities.PayOutPaymentDetailsBankWire;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Component
public abstract class CommonsMangopayServiceImpl<T extends UserMangoPay, S extends Serializable> implements CommonsMangoPayService<T, S> {

    protected String clientId;
    protected String apiToken;
    protected String baseUrl;

    /**
     * @param clientId the Mango pay client Id needed to create the API (never <code>null</code>)
     * @param token    the Mango pay api Token needed to create the API (never <code>null</code>)
     * @param baseUrl  the mango pay Url can be either (for test <a>https://api.sandbox.mangopay.com<a/> )(for production <a>https://api.mangopay.com</a>)
     */
    public CommonsMangopayServiceImpl(String clientId, String token, String baseUrl) {
        this.apiToken = token;
        this.baseUrl = baseUrl;
        this.clientId = clientId;
    }

    /**
     * @return the mango pay api client id
     */
    public abstract String getClienId();

    /**
     * @return the mango pay api token
     */
    public abstract String getApiToken();

    /**
     * @return the mango pay api base url
     */
    public abstract String getBaseUrl();


    /**
     * @return mango pay api connection
     */
    @Override
    public MangoPayApi getApiConnection() {
        MangoPayApi mangoPayApi = new MangoPayApi();
        Configuration configuration = new Configuration();
        configuration.setClientId(clientId);
        configuration.setClientPassword(apiToken);
        configuration.setBaseUrl(baseUrl);
        mangoPayApi.setConfig(configuration);
        return mangoPayApi;
    }


    @Override
    public UserNatural createMangoPayUser(UserNatural userNatural) throws Exception {
        MangoPayApi api = getApiConnection();
        return (UserNatural) api.getUserApi().create(userNatural);
    }


    @Override
    public Wallet getWalletByUser(T user) throws Exception {
        MangoPayApi mangoPayApi = getApiConnection();
        List<Wallet> wallets = mangoPayApi.getUserApi().getWallets(user.getMangoPayId());
        if (!wallets.isEmpty()) {
            return wallets.get(0);
        } else {
            ArrayList<String> ownersWallet = new ArrayList<>();
            Wallet wallet = new Wallet();
            ownersWallet.add(user.getMangoPayId());
            wallet.setOwners(ownersWallet);
            wallet.setDescription("Default Description");
            wallet.setCurrency(CurrencyIso.EUR);
            wallet = mangoPayApi.getWalletApi().create(wallet);
            return wallet;
        }
    }



    @Override
    public PayOut createPayOutToBankAccount(T user, int amount, String referencePayOut) throws Exception {
        MangoPayApi mangoPayApi = getApiConnection();
        PayOut payOut = new PayOut();
        PayOutPaymentDetailsBankWire payOutPaymentDetailsBankWire = new PayOutPaymentDetailsBankWire();
        payOutPaymentDetailsBankWire.setBankAccountId(mangoPayApi.getUserApi().getBankAccounts(user.getMangoPayId()).get(0).getId());
        payOutPaymentDetailsBankWire.setBankWireRef(referencePayOut);
        payOut.setMeanOfPaymentDetails(payOutPaymentDetailsBankWire);
        Money money = new Money();
        Wallet wallet = getWalletByUser(user);
        money.setCurrency(wallet.getCurrency());
        money.setAmount(amount);
        payOut.setDebitedFunds(money);
        Money frees = new Money();
        frees.setCurrency(wallet.getCurrency());
        frees.setAmount(0);
        payOut.setFees(frees);
        payOut.setDebitedWalletId(wallet.getId());
        payOut.setAuthorId(user.getMangoPayId());
        payOut.setPaymentType(PayOutPaymentType.BANK_WIRE);
        payOut = mangoPayApi.getPayOutApi().create(payOut);
        return payOut;
    }


    @Override
    public BankAccount getUserBankAccount(T user) throws Exception {
        MangoPayApi mangoPayApi = getApiConnection();
        List<BankAccount> bankAccounts = mangoPayApi.getUserApi().getBankAccounts(user.getMangoPayId());
        if (!bankAccounts.isEmpty()) {
            return bankAccounts.get(0);
        } else {
            return null;
        }
    }


    @Override
    public PayIn createPayInWebByCard(String authorId, T user, int amount, int free, CardType cardType, String returnedUrl) throws Exception {
        PayIn payIn = new PayIn();
        Money money = new Money();
        Money frees = new Money();
        MangoPayApi mangoPayApi = getApiConnection();
        money.setAmount(amount * 100);
        money.setCurrency(CurrencyIso.EUR);
        frees.setCurrency(CurrencyIso.EUR);
        frees.setAmount(free);
        payIn.setDebitedFunds(money);
        payIn.setFees(frees);
        payIn.setCreditedWalletId(getWalletByUser(user).getId());
        payIn.setAuthorId(authorId);
        payIn.setPaymentType(PayInPaymentType.CARD);
        PayInPaymentDetailsCard payInPaymentDetailsCard = new PayInPaymentDetailsCard();
        payInPaymentDetailsCard.setCardType(cardType);
        payIn.setPaymentDetails(payInPaymentDetailsCard);
        payIn.setExecutionType(PayInExecutionType.WEB);
        PayInExecutionDetailsWeb payInExecutionDetailsWeb = new PayInExecutionDetailsWeb();
        payInExecutionDetailsWeb.setReturnUrl(returnedUrl);
        payInExecutionDetailsWeb.setCulture(CultureCode.FR);
        payIn.setExecutionDetails(payInExecutionDetailsWeb);
        payIn = mangoPayApi.getPayInApi().create(payIn);
        return payIn;
    }

}
