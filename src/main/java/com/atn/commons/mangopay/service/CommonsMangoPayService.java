package com.atn.commons.mangopay.service;

import com.atn.commons.mangopay.entities.UserMangoPay;
import com.mangopay.MangoPayApi;
import com.mangopay.core.enumerations.CardType;
import com.mangopay.entities.*;

import java.io.Serializable;


/**
 * @author <a href="mailto:montassat.bouagina@allence-tunisie.com">Monta</a>
 * @version $Id: ModelMango.java Monta $
 * @since commons version 3.76
 */

public interface CommonsMangoPayService<T extends UserMangoPay, S extends Serializable> {

    /**
     * Gets the mangoPay Api connection
     * the mangopay api clientId and token should be specified as  commons.mango.api.client and commons.mango.api.token In the resources config.properties
     * <p>
     * a value commons.mango.api.url should be specified as https://api.mangopay.com if it's a production api otherwise it's  sandbox
     *
     * @return a <code> MangoPayApi </code>
     * create connection to mango api server
     * @see com.mangopay.core
     */

    MangoPayApi getApiConnection();


    /**
     * <p>This method creat a connection to the mango pay api.
     * the connection will be created before every service</p>
     *
     * @param userNatural user object
     * @return <code>userNatural</code> Object from (com.mangopay)
     * @throws  Exception in case there's a problem with null attribute in
     * UserNatural.
     * More Details in this <a href="https://docs.mangopay.com/endpoints/v2.01/users#e255_create-a-natural-user">Link</a>
     * @see com.mangopay.entities.UserNatural
     */

    UserNatural createMangoPayUser(UserNatural userNatural) throws Exception;

    /**
     * <p>This method creates a payout from the users's Wallet to a bank account.
     * The user must have a bank account in mango pay</p>
     * @param user  the user must contain a mango pay id (mango pay Id not <code>null</code>)
     * @param amount  describes the amout you want to withdraw
     * @param referencePayOut describes the payout Reference
     * @return <code>Payout</code> Object
     * @throws Exception if the user doesn't have a mango pay id or a bank account
     * @see com.mangopay.entities.BankAccount
     */

    PayOut createPayOutToBankAccount(T user, int amount, String referencePayOut) throws Exception;

    /**
     * <p> This method get the user's wallet if it Exist else it creates a wallet for the user.
     * the description of the wallet is "Default Description"</p>
     * @param user is the user in question must have valid mangoPayID
     * @return <code>Wallet</code>
     * @throws Exception in case User don't have a valid mangopay Id
     */

    Wallet getWalletByUser(T user) throws Exception;



    /**
     * <p>This method gets the user's bank account if it exist otherwise it creates
     * creates a bank account to the user</p>
     * @param user the user must have  mangoPayId
     * @return <code>BankAccount</code>
     * @throws Exception if the user doesn't have a mango pay id
     */
    BankAccount getUserBankAccount(T user) throws Exception;

    /**
     *
     * @param authorId reference to a user's mango py Id who authered the transfert
     * @param user the owner of the wallet that will be credited
     * @param amount  describes the amount tht will be debited
     * @param free the amount of feed
     * @param cardType describes the  type of card that is being used
     * @see com.mangopay.entities.Card
     * @param returnedUrl a string web url to be returned after method succeeds
     * @return <code>PayIn</code> object.
     * @throws Exception if the one of the information is invalid
     */

    PayIn createPayInWebByCard(String authorId, T user, int amount, int free, CardType cardType, String returnedUrl) throws Exception;
}
