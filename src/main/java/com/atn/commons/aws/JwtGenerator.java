package com.atn.commons.aws;

import java.io.FileWriter;
import java.io.IOException;

public class JwtGenerator {

    public static void main (String[] args){

        String userPoolID = args[0];
        String clientid = args[1];
        String secretKey = args[2];

        String userName = args[3];
        String Password = args[4];

        String filePath = args[5];


        AuthenticationHelper AuthenticationHelper = new AuthenticationHelper(userPoolID, clientid, secretKey);
        AuthenticationHelper.initiateUserSrpAuthRequest(userName);
        AuthenticationHelper.PerformSRPAuthentication(userName, Password);
        System.out.println(AuthenticationHelper.getChallengeRequest());


        try {
            FileWriter myWriter = new FileWriter(filePath);
            myWriter.write(" [{\"Autorisation\":\"" + AuthenticationHelper.getAuthresult()+ "\"}]");
            myWriter.close();
            System.out.println("Successfully wrote to the file.");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

    }



}
