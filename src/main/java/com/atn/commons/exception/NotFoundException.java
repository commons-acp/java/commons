package com.atn.commons.exception;

public class NotFoundException extends ApiException {
    private BodyErrors httpError;

    public NotFoundException(String message, BodyErrors error) {
        super(message);
        this.httpError = error;

    }

    @Override
    protected int getCode() {
        return httpError.getCode();
    }
}
