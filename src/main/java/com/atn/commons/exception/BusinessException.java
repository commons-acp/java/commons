package com.atn.commons.exception;

/**
 * Exception Metier
 * 
 * @author Mohamed Arbi Ben Slimane
 *
 * @Email benslimane.arbi@gmail.com
 *
 * @Date 16 avr. 2017 at 11:16:25
 */
public class BusinessException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7418816578877431920L;

	public BusinessException(String message, Throwable cause) {
		super(message, cause);
	}

}
