package com.atn.commons.exception;

import lombok.Data;

public abstract class ApiException extends RuntimeException {

    public ApiException(final String message) {
        super(message);
    }

    public HttpErrorResponse getError() {
        return new HttpErrorResponse(getCode(), getMessage());
    }

    protected abstract int getCode();


}
