package com.atn.commons.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.*;

@RestControllerAdvice
@Slf4j
public class RestApiExceptionHandler {

    @ExceptionHandler(NotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public Object handle(final NotFoundException ex) {
        log.error("NotFoundException : " + ex.getMessage(), ex.getClass());
        NotFoundException exception = new NotFoundException(ex.getMessage(), BodyErrors.NOT_FOUND);
        return exception.getError();
    }

    @ExceptionHandler(BadRequestException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public Object handle(final BadRequestException ex) {
        log.error("BadRequestException : " + ex.getMessage(), ex.getClass());
        BadRequestException exception = new BadRequestException(ex.getMessage(), BodyErrors.BAD_REQUEST);
        return exception.getError();
    }


    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(HttpMessageNotReadableException.class)
    @ResponseBody
    public Object handleException() {
        return new HttpErrorResponse(BodyErrors.JSON_BODY_ERROR.getCode(), "JSON Body Error , can't be Serialized");
    }
}
