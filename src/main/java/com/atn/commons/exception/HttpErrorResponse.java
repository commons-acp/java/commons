package com.atn.commons.exception;

import lombok.Data;

@Data
public class HttpErrorResponse {

    public HttpErrorResponse() {
    }

    public HttpErrorResponse(int code, String message) {
        this.code = code;
        this.message = message;
    }

    private int code;
    private String message;
}
