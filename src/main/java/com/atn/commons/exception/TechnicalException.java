package com.atn.commons.exception;

/**
 * Exception technique
 * 
 * @author Mohamed Arbi Ben Slimane
 *
 * @Email benslimane.arbi@gmail.com
 *
 * @Date 16 avr. 2017 at 11:17:39
 */
public class TechnicalException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3188909331963043882L;

	public TechnicalException(String message, Throwable cause) {
		super(message, cause);
	}

}
