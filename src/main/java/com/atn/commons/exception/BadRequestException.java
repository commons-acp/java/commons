package com.atn.commons.exception;

public class BadRequestException extends ApiException {
    private BodyErrors httpError;

    public BadRequestException(String message, BodyErrors error) {
        super(message);
        this.httpError = error;

    }

    @Override
    protected int getCode() {
        return httpError.getCode();
    }
}
