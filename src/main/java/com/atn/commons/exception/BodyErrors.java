package com.atn.commons.exception;

public enum BodyErrors {


    FATAL_ERROR(999),
    NOT_FOUND(998),
    BAD_PARAMS(996),
    JSON_BODY_ERROR(995),
    BAD_REQUEST(997);

    public int getCode() {
        return code;
    }

    private final int code;

    BodyErrors(int code) {
        this.code = code;
    }
}
